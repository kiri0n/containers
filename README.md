# Containers

1. [Technical description](#technical-description)
2. [Description](#description)
3. [List](#list)
4. [Map](#map)
5. [Queue](#queue)
6. [Set](#set)
7. [Stack](#stack)
8. [Vector](#vector)
9. [Array](#array)
10. [Multiset](#multiset)

## Technical description
- The program was developed in C++ standard C++17
- The program code is written in accordance with Google style
- Classes are implemented by templates
- Iterators implemented
- Full unit test coverage

## Description
S21_containers.h'library classes implemented:
- list
- map
- queue
- set
- stack
- vector

S21_containersplus.h'library classes implemented:
- array
- multiset

Iterator has the following operations:

- `*iter`: gets the element pointed to by the iterator;

- `++iter`: moves the iterator forward to the next element

- `--iter`: moves the iterator backwards to the previous element;

- `iter1 == iter2`: two iterators are equal if they point to the same element

- `iter1 != iter2`: two iterators are not equal if they point to different elements

Implementation of the modified `emplace` methods

| Modifiers | Definition | Containers |
|----------------|-------------------------------------------------| -------------------------------------------|
| `iterator emplace(const_iterator pos, Args&&... args)` | inserts new elements into the container directly before `pos` | List, Vector |
| `void emplace_back(Args&&... args)` | appends new elements to the end of the container  | List, Vector, Queue |
| `void emplace_front(Args&&... args)` | appends new elements to the top of the container | List, Stack |
| `vector<std::pair<iterator,bool>> emplace(Args&&... args)`          | inserts new elements into the container  | Map, Set, Multiset |

### List
#### Constructors
`list()`, `list(size_type n)`, `list(initializer_list<value_type> const &items)`, `list(const list &other)`, `list(list &&other)`
#### Methods & operators
| Member type      | Definition                                      |
|----------------|-------------------------------------------------|
| `value_type` | `T` defines the type of an element (T is a template parameter) |
| `reference` | `T &` defines the type of the reference to an element |
| `const_reference`| `const T &` defines the type of the constant reference |
| `iterator` | internal class `ListIterator<T>` defines the type for iterating through the container |
| `const_iterator` | internal class `ListConstIterator<T>` defines the constant type for iterating through the container |
| `size_type` | `size_t` defines the type of the container size (standard type is size_t) |


| Function      | Definition                                      |
|----------------|-------------------------------------------------|
| `const_reference front()` | access the first element |
| `const_reference back()` | access the last element |
| `iterator begin()`| returns an iterator to the beginning |
| `iterator end()` | returns an iterator to the end |
| `bool empty()` | checks whether the container is empty |
| `size_type size()` | returns the number of elements |
| `void clear()` | clears the contents |
| `iterator insert(iterator pos, const_reference value)` | inserts elements into concrete pos and returns the iterator that points to the new element |
| `void erase(iterator pos)` | erases an element at pos |
| `void push_back(const_reference value)` | adds an element to the end |
| `void pop_back()` | removes the last element |
| `void push_front(const_reference value)` | adds an element to the head |
| `void pop_front()` | removes the first element |
| `void swap(list& other)` | swaps the contents |
| `void merge(list& other)` | merges two sorted lists |
| `void splice(const_iterator pos, list& other)` | transfers elements from list other starting from pos |
| `void reverse()` | reverses the order of the elements |
| `void unique()` | removes consecutive duplicate elements |
| `void sort()` | sorts the elements |

### Map
#### Constructors
`map()`, `map(initializer_list<value_type> const &items)`, `map(const map &other)`, `map(map &&other)`
#### Methods & operators
| Member type      | Definition                                      |
|----------------|-------------------------------------------------|
| `key_type` | `Key` the first template parameter (Key) |
| `mapped_type` | `T` the second template parameter (T) |
| `value_type` | `std::pair<const key_type,mapped_type>` Key-value pair |
| `reference` | `value_type &` defines the type of the reference to an element |
| `const_reference`| `const value_type &` defines the type of the constant reference |
| `iterator` | internal class `MapIterator<K, T>` as internal iterator of tree subclass; defines the type for iterating through the container |
| `const_iterator` | internal class `MapConstIterator<K, T` das internal const iterator of tree subclass; defines the constant type for iterating through the container |
| `size_type` | `size_t` defines the type of the container size (standard type is size_t) |


| Methods | Definition |
|------------------------|-----------------------------------------|
| `T& at(const Key& key)` | access a specified element with bounds checking |
| `T& operator[](const Key& key)` | access or insert specified element |
| `iterator begin()` | returns an iterator to the beginning |
| `iterator end()` | returns an iterator to the end |
| `bool empty()` | checks whether the container is empty |
| `size_type size()` | returns the number of elements |
| `size_type max_size()` | returns the maximum possible number of elements |
| `void clear()` | clears the contents |
| `std::pair<iterator, bool> insert(const value_type& value)` | inserts a node and returns an iterator to where the element is in the container and bool denoting whether the insertion took place |
| `std::pair<iterator, bool> insert(const Key& key, const T& obj)` | inserts a value by key and returns an iterator to where the element is in the container and bool denoting whether the insertion took place |
| `std::pair<iterator, bool> insert_or_assign(const Key& key, const T& obj);` | inserts an element or assigns to the current element if the key already exists |
| `void erase(iterator pos)` | erases an element at pos |
| `void swap(map& other)` | swaps the contents |
| `void merge(map& other);` | splices nodes from another container |
| `bool contains(const Key& key)` | checks if there is an element with key equivalent to key in the container |\

### Queue
#### Constructors
`queue()`, `queue(initializer_list<value_type> const &items)`, `queue(const queue &other)`, `queue(queue &&other)`
#### Methods & operators
| Member type      | Definition                                       |
|------------------|--------------------------------------------------|
| `value_type` | `T` the template parameter T |
| `reference` | `T &` defines the type of the reference to an element |
| `const_reference` | `const T &` defines the type of the constant reference |
| `size_type` | `size_t` defines the type of the container size (standard type is size_t) |


| Methods | Definition |
|------------------------|-----------------------------------------|
| `const_reference front()` | access the first element |
| `const_reference back()` | access the last element |
| `bool empty()` | checks whether the container is empty |
| `size_type size()` | returns the number of elements |
| `void push(const_reference value)` | inserts an element at the end |
| `void pop()` | removes the first element |
| `void swap(queue& other)` | swaps the contents |


### Set
#### Constructors
`set()`, `set(initializer_list<value_type> const &items)`, `set(const set &other)`, `set(set &&other)`
#### Methods & operators
| Member type      | Definition                                       |
|------------------|--------------------------------------------------|
| `key_type` | `Key` the first template parameter (Key) |
| `value_type` | `Key` value type (the value itself is a key) |
| `reference` | `value_type &` defines the type of the reference to an element |
| `const_reference` | `const value_type &` defines the type of the constant reference |
| `iterator` | internal class `SetIterator<T>` or `BinaryTree::iterator` as the internal iterator of tree subclass; defines the type for iterating through the container |
| `const_iterator` | internal class `SetConstIterator<T>` or `BinaryTree::const_iterator` as the internal const iterator of tree subclass; defines the constant type for iterating through the container |
| `size_type` | `size_t` defines the type of the container size (standard type is size_t) |


| Methods | Definition |
|------------------------|-----------------------------------------|
| `iterator begin()` | returns an iterator to the beginning |
| `iterator end()` | returns an iterator to the end |
| `bool empty()` | checks whether the container is empty |
| `size_type size()` | returns the number of elements |
| `size_type max_size()` | returns the maximum possible number of elements |
| `void clear()` | clears the contents |
| `std::pair<iterator, bool> insert(const value_type& value)` | inserts a node and returns an iterator to where the element is in the container and bool denoting whether the insertion took place |
| `void erase(iterator pos)` | erases an element at pos |
| `void swap(set& other)` | swaps the contents |
| `void merge(set& other);` | splices nodes from another container |
| `iterator find(const Key& key)` | finds an element with a specific key |
| `bool contains(const Key& key)` | checks if the container contains an element with a specific key |

### Stack
#### Constructors
`stack()`, `stack(initializer_list<value_type> const &items)`, `stack(const stack &other)`, `stack(stack &&other)`
#### Methods & operators
| Member type      | Definition                                       |
|------------------|--------------------------------------------------|
| `value_type` | `T` the template parameter T |
| `reference` | `T &` defines the type of the reference to an element |
| `const_reference` | `const T &` defines the type of the constant reference |
| `size_type` | `size_t` defines the type of the container size (standard type is size_t) |

| Methods | Definition |
|------------------------|-----------------------------------------|
| `const_reference top()` | accesses the top element |
| `bool empty()`  | checks whether the container is empty |
| `size_type size()` | returns the number of elements |
| `void push(const_reference value)` | inserts an element at the top |
| `void pop()` | removes the top element                        |
| `void swap(stack& other)` | swaps the contents |

### Vector
#### Constructors
`vector()`,`vector(size_type n)`, `vector(initializer_list<value_type> const &items)`, `vector(const vector &other)`, `vector(vector &&other)`
#### Methods & operators
| Member type      | Definition                                       |
|------------------|--------------------------------------------------|
| `value_type` | `T` defines the type of the element (T is template parameter) |
| `reference` | `T &` defines the type of the reference to an element |
| `const_reference` | `const T &` defines the type of the constant reference |
| `iterator` | `T *` or internal class `VectorIterator<T>` defines the type for iterating through the container |
| `const_iterator` | `const T *` or internal class `VectorConstIterator<T>` defines the constant type for iterating through the container |
| `size_type` | `size_t` defines the type of the container size (standard type is size_t) |

| Methods | Definition |
|------------------------|-----------------------------------------|
| `reference at(size_type pos)` | access a specified element with bounds checking |
| `reference operator[](size_type pos);` | access a specified element |
| `const_reference front()` | access the first element |
| `const_reference back()` | access the last element |
| `T* data()` | direct access the underlying array |
| `iterator begin()` | returns an iterator to the beginning |
| `iterator end()` | returns an iterator to the end |
| `bool empty()` | checks whether the container is empty |
| `size_type size()` | returns the number of elements |
| `size_type max_size()` | returns the maximum possible number of elements |
| `void reserve(size_type size)` | allocate storage of size elements and copies current array elements to a newely allocated array |
| `size_type capacity()` | returns the number of elements that can be held in currently allocated storage |
| `void shrink_to_fit()` | reduces memory usage by freeing unused memory |
| `void clear()` | clears the contents |
| `iterator insert(iterator pos, const_reference value)` | inserts elements into concrete pos and returns the iterator that points to the new element |
| `void erase(iterator pos)` | erases an element at pos |
| `void push_back(const_reference value)` | adds an element to the end |
| `void pop_back()` | removes the last element |
| `void swap(vector& other)` | swaps the contents |

### Array
#### Constructors
`array()`, `array(initializer_list<value_type> const &items)`, `array(const array &other)`, `array(array &&other)`
#### Methods & operators
| Member type      | Definition                                       |
|------------------|--------------------------------------------------|
| `value_type` | `T` defines the type of an element (T is template parameter) |
| `reference` | `T &` defines the type of the reference to an element |
| `const_reference` | `const T &` defines the type of the constant reference |
| `iterator` | `T *` defines the type for iterating through the container |
| `const_iterator` | `const T *` defines the constant type for iterating through the container |
| `size_type` | `size_t` defines the type of the container size (standard type is size_t) |

| Methods | Definition |
|------------------------|-----------------------------------------|
| `reference at(size_type pos)` | access a specified element with bounds checking |
| `reference operator[](size_type pos)` | access a specified element |
| `const_reference front()` | access the first element |
| `const_reference back()` | access the last element |
| `iterator data()` | direct access to the underlying array |
| `iterator begin()` | returns an iterator to the beginning |
| `iterator end()` | returns an iterator to the end |
| `bool empty()` | checks whether the container is empty |
| `size_type size()` | returns the number of elements |
| `size_type max_size()` | returns the maximum possible number of elements |
| `void swap(array& other)` | swaps the contents |
| `void fill(const_reference value);` | assigns the given value to all elements in the container. |

### Multiset
#### Constructors
`multiset()`, `multiset(initializer_list<value_type> const &items)`, `multiset(const multiset &other)`, `multiset(multiset &&other)`
#### Methods & operators
| Member type      | Definition                                       |
|------------------|--------------------------------------------------|
| `key_type` | `Key` the first template parameter (Key) |
| `value_type` | `Key` value type (the value itself is a key) |
| `reference` | `value_type &` defines the type of the reference to an element |
| `const_reference` | `const value_type &` defines the type of the constant reference |
| `iterator` | internal class `MultisetIterator<T>` as internal iterator of tree subclass; defines the type for iterating through the container |
| `const_iterator` | internal class `MultisetConstIterator<T>` or as internal const iterator of tree subclass; defines the constant type for iterating through the container |
| `size_type` | `size_t` defines the type of the container size (standard type is size_t) |

| Methods | Definition |
|------------------------|-----------------------------------------|
| `iterator begin()` | returns an iterator to the beginning |
| `iterator end()` | returns an iterator to the end |
| `bool empty()` | checks whether the container is empty |
| `size_type size()` | returns the number of elements |
| `size_type max_size()` | returns the maximum possible number of elements |
| `void clear()` | clears the contents |
| `iterator insert(const value_type& value)` | inserts a node and returns an iterator to where the element is in the |
| `void erase(iterator pos)` | erases an element at pos |
| `void swap(multiset& other)` | swaps the contents |
| `void merge(multiset& other)` | splices nodes from another container |
| `size_type count(const Key& key)` | returns the number of elements matching a specific key |
| `iterator find(const Key& key)` | finds element with a specific key |
| `bool contains(const Key& key)` | checks if the container contains element with a specific key |
| `std::pair<iterator,iterator> equal_range(const Key& key)` | returns range of elements matching a specific key |
| `iterator lower_bound(const Key& key)` | returns an iterator to the first element not less than the given key |
| `iterator upper_bound(const Key& key)` | returns an iterator to the first element greater than the given key |

