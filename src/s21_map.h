#ifndef CONTAINER_S21_MAP_H_
#define CONTAINER_S21_MAP_H_

#include "s21_tree.h"

namespace s21 {

template <typename K, typename T>
class map : public Tree<std::pair<K, T>> {
 public:
  using thisclass = map;
  using key_type = K;
  using mapped_type = T;
  using value_type = std::pair<K, T>;
  using reference = value_type&;
  using const_reference = const value_type&;
  using size_type = size_t;
  using iterator = typename Tree<std::pair<K, T>>::iterator;
  using Node = NodeTree<std::pair<K, T>>;

  map();
  explicit map(std::initializer_list<value_type> const& items);
  map(const map& s);
  map(map&& s);
  ~map();
  map operator=(map&& s);

  std::pair<iterator, bool> insert(const value_type& value);
  std::pair<iterator, bool> insert(const K& key, const T& obj);
  std::pair<iterator, bool> insert_or_assign(const K& key, const T& obj);

  T& at(const K& key);
  T& operator[](const K& key);
  bool contains(const K& key);

  iterator find_key(const key_type& value, Node* ptrf);

  template <typename... Args>
  std::vector<std::pair<iterator, bool>> emplace(Args&&... args);
};

#include "s21_map.inl"
}  // namespace s21

#endif // CONTAINER_S21_MAP_H_
