template <typename K, typename T>
map<K, T>::map() {
  NodeTree<std::pair<K, T>> *tmp = this->ptr;
  this->ptr = tmp->Construct();
}

template <typename K, typename T>
map<K, T>::map(std::initializer_list<value_type> const &items)
    : map<K, T>::map() {
  for (auto i = items.begin(); i != items.end(); ++i) {
    this->insert(*i);
  }
}

template <typename K, typename T>
map<K, T>::map(const map &s) : map<K, T>::map() {
  iterator it = s.begin();
  while (it != s.end()) {
    insert(*it);
    ++it;
  }
}

template <typename K, typename T>
map<K, T>::map(map &&s) {
  this->ptr = s->ptr;
  this->ptr = nullptr;
}

template <typename K, typename T>
map<K, T>::~map() {
  this->deleteTree();
}

template <typename K, typename T>
map<K, T> map<K, T>::operator=(map &&s) {
  this->deleteTree();
  this->ptr = s->ptr;
  s->ptr = nullptr;
  return *this;
}

template <typename K, typename T>
T &map<K, T>::at(const K &key) {
  iterator it;
  it = find_key(key, this->ptr);
  if (it.iter->Key.first != key) throw std::out_of_range("Not find Key");
  return it.iter->Key.second;
}

template <typename K, typename T>
T &map<K, T>::operator[](const K &key) {
  iterator it;
  it = find_key(key, this->ptr);
  if (it.iter->status == end_node || it.iter->Key.first != key) {
    T tmp = this->ptr->Key.second;
    it = insert(key, tmp).first;
  }
  return it.iter->Key.second;
}

template <typename K, typename T>
std::pair<typename map<K, T>::iterator, bool> map<K, T>::insert(
    const value_type &value) {
  return this->sup_insert(value);
}

template <typename K, typename T>
std::pair<typename map<K, T>::iterator, bool> map<K, T>::insert(const K &key,
                                                                const T &obj) {
  std::pair<K, T> tmp = std::make_pair(key, obj);
  return this->sup_insert(tmp);
}

template <typename K, typename T>
std::pair<typename map<K, T>::iterator, bool> map<K, T>::insert_or_assign(
    const K &key, const T &obj) {
  std::pair<iterator, bool> result;
  iterator it = find_key(key, this->ptr);
  if (it.iter->status != end_node && it.iter->Key.first == key) {
    it.iter->Key.second = obj;
    result = std::make_pair(it, true);
  } else {
    result = insert(key, obj);
  }
  return result;
}

template <typename K, typename T>
typename map<K, T>::iterator map<K, T>::find_key(const key_type &value,
                                                 Node *ptrf) {
  Node *find = ptrf;
  Node *last = ptrf;
  bool next_path = this->size_tree != 0;
  while (next_path == true) {
    if (value == find->Key.first) {
      last = find;
      next_path = false;
    }
    if (value < find->Key.first) {
      if (find->left != nullptr) {
        find = find->left;
      } else {
        next_path = false;
      }
    } else {
      if (find->right != nullptr && find->right->status != end_node) {
        find = find->right;
      } else {
        next_path = false;
      }
    }
  }
  iterator res = this->begin();
  if (last->Key.first == value)
    res.iter = last;
  else
    res.iter = find;
  return res;
}

template <typename K, typename T>
template <typename... Args>
std::vector<std::pair<typename map<K, T>::iterator, bool>> map<K, T>::emplace(
    Args &&...args) {
  std::vector<std::pair<iterator, bool>> result;
  std::pair<K, T> arr[] = {args...};
  size_t x = 0;
  for (std::pair<K, T> i : arr) {
    result.push_back(insert(i));
    x++;
  }
  return result;
}

template <typename K, typename T>
bool map<K, T>::contains(const K &key) {
  iterator tmp = find_key(key, this->ptr);
  return tmp.iter->Key.first == key;
}
