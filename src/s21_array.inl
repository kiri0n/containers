template <typename T, size_t V>
array<T, V>::array() {}

template <typename value_type, size_t V>
array<value_type, V>::array(std::initializer_list<value_type> const &items) {
  if (items.size() > V) {
    throw std::out_of_range("variables oversize");
  }
  int i = 0;
  for (auto element : items) {
    container_[i] = element;
    i++;
  }
}

template <typename value_type, size_t V>
array<value_type, V>::array(const array &a) {
  std::copy(a.const_begin(), a.const_end(), container_);
}

template <typename value_type, size_t V>
array<value_type, V>::array(array &&a) {
  if (this != &a) {
    std::move(a.begin(), a.end(), container_);
  }
}

template <typename value_type, size_t V>
array<value_type, V>::~array() {}

template <typename value_type, size_t V>
array<value_type, V> &array<value_type, V>::operator=(array &a) {
  std::copy(a.const_begin(), a.const_end(), container_);

  return *this;
}

template <typename value_type, size_t V>
array<value_type, V> &array<value_type, V>::operator=(array &&a) {
  if (this != &a) {
    std::move(a.begin(), a.end(), container_);
  }

  return *this;
}

template <typename value_type, size_t V>
typename array<value_type, V>::reference array<value_type, V>::at(
    size_type pos) {
  if (pos >= V) {
    throw std::out_of_range("position outside range");
  }

  return container_[pos];
}

template <typename value_type, size_t V>
typename array<value_type, V>::reference array<value_type, V>::operator[](
    size_type pos) {
  return container_[pos];
}

template <typename value_type, size_t V>
typename array<value_type, V>::const_reference array<value_type, V>::front()
    const {
  if (this->empty()) {
    throw std::out_of_range("sega");
  }

  return container_[0];
}

template <typename value_type, size_t V>
typename array<value_type, V>::const_reference array<value_type, V>::back()
    const {
  if (this->empty()) {
    throw std::out_of_range("sega");
  }

  return container_[V - 1];
}

template <typename T, size_t V>
typename array<T, V>::iterator array<T, V>::data() {
  return container_;
}

template <typename value_type, size_t V>
typename array<value_type, V>::iterator array<value_type, V>::begin() {
  return container_;
}

template <typename value_type, size_t V>
typename array<value_type, V>::iterator array<value_type, V>::end() {
  return &container_[V];
}

template <typename value_type, size_t V>
typename array<value_type, V>::const_iterator
array<value_type, V>::const_begin() const {
  return container_;
}

template <typename value_type, size_t V>
typename array<value_type, V>::const_iterator array<value_type, V>::const_end()
    const {
  return &container_[V];
}

template <typename value_type, size_t V>
bool array<value_type, V>::empty() const {
  return V > 0 ? 0 : 1;
}

template <typename value_type, size_t V>
typename array<value_type, V>::size_type array<value_type, V>::size() const {
  return V;
}

template <typename value_type, size_t V>
typename array<value_type, V>::size_type array<value_type, V>::max_size()
    const {
  return V;
}

template <typename value_type, size_t V>
void array<value_type, V>::swap(array &other) {
  std::swap(container_, other.container_);
}

template <typename value_type, size_t V>
void array<value_type, V>::fill(const_reference value) {
  for (size_t i = 0; i < V; ++i) {
    container_[i] = value;
  }
}
