template <typename value_type, typename allocation_type>
vector<value_type, allocation_type>::vector()
    : c_size_(0), c_capacity_(0), container_(nullptr) {}

template <typename value_type, typename allocation_type>
vector<value_type, allocation_type>::vector(size_type n)
    : c_size_(n), c_capacity_(n) {
  container_ = c_allocation_.allocate(n);
  for (size_type i = 0; i < c_size_; ++i) {
    c_allocation_.construct(container_ + i, value_type());
  }
}

template <typename value_type, typename allocation_type>
vector<value_type, allocation_type>::vector(
    std::initializer_list<value_type> const &items)
    : vector(items.size()) {
  int i = 0;
  for (auto element : items) {
    container_[i] = element;
    i++;
  }
  c_size_ = items.size();
  c_capacity_ = items.size();
}

template <typename value_type, typename allocation_type>
vector<value_type, allocation_type>::vector(const vector &v)
    : vector(v.c_size_) {
  std::copy(v.container_, v.container_ + v.c_size_, container_);
}

template <typename value_type, typename allocation_type>
vector<value_type, allocation_type>::vector(vector &&v) {
  if (this != &v) {
    *this = std::move(v);
    v.container_ = nullptr;
    v.c_size_ = 0;
    v.c_capacity_ = 0;
  }
}

template <typename value_type, typename allocation_type>
vector<value_type, allocation_type>::~vector() {
  if (container_) {
    c_allocation_.deallocate(container_, c_capacity_);
  }
  c_size_ = 0;
  c_capacity_ = 0;
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::vector &
vector<value_type, allocation_type>::operator=(const vector &v) {
  if (container_) this->~vector();
  c_capacity_ = v.c_capacity_;
  c_size_ = v.c_size_;
  container_ = c_allocation_.allocate(v.c_size_);
  std::copy(v.container_, v.container_ + v.c_size_, container_);

  return *this;
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::vector &
vector<value_type, allocation_type>::operator=(vector &&v) {
  if (this != &v) {
    c_capacity_ = v.c_capacity_;
    c_size_ = v.c_size_;
    container_ = v.container_;

    v.container_ = nullptr;
    v.c_size_ = 0;
    v.c_capacity_ = 0;
  }

  return *this;
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::reference
vector<value_type, allocation_type>::at(size_type pos) {
  if (pos >= c_size_) {
    throw std::out_of_range("position outside range");
  }

  return container_[pos];
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::reference
vector<value_type, allocation_type>::operator[](size_type pos) {
  return *(container_ + pos);
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::const_reference
vector<value_type, allocation_type>::front() const {
  if (this->empty()) {
    throw std::out_of_range("container is empty - sega");
  }

  return container_[0];
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::const_reference
vector<value_type, allocation_type>::back() const {
  if (this->empty()) {
    throw std::out_of_range("container is empty - sega");
  }

  return container_[c_size_ - 1];
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::iterator
vector<value_type, allocation_type>::data() {
  return container_;
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::iterator
vector<value_type, allocation_type>::begin() {
  return container_;
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::iterator
vector<value_type, allocation_type>::end() {
  return container_ + c_size_;
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::const_iterator
vector<value_type, allocation_type>::const_begin() {
  return container_;
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::const_iterator
vector<value_type, allocation_type>::const_end() {
  return container_ + c_size_;
}

template <typename value_type, typename allocation_type>
bool vector<value_type, allocation_type>::empty() const {
  return c_size_ > 0 ? 0 : 1;
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::size_type
vector<value_type, allocation_type>::size() const {
  return c_size_;
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::size_type
vector<value_type, allocation_type>::max_size() const {
  return c_allocation_.max_size();
}

template <typename value_type, typename allocation_type>
void vector<value_type, allocation_type>::reserve(size_type size) {
  if (size > max_size()) {
    throw std::length_error("impossible size");
  }
  if (size >= c_size_) {
    iterator temporary = c_allocation_.allocate(size);
    for (size_type i = 0; i < c_size_; ++i) {
      temporary[i] = std::move(container_[i]);
    }

    c_allocation_.deallocate(container_, c_capacity_);
    c_capacity_ = size;
    container_ = temporary;
  }
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::size_type
vector<value_type, allocation_type>::capacity() const {
  return c_capacity_;
}

template <typename value_type, typename allocation_type>
void vector<value_type, allocation_type>::shrink_to_fit() {
  if (c_size_ != c_capacity_) {
    reserve(c_size_);
  }
}

template <typename value_type, typename allocation_type>
void vector<value_type, allocation_type>::clear() {
  for (size_type i = 0; i < c_size_; ++i) {
    c_allocation_.destroy(container_ + i);
  }
  c_size_ = 0;
}

template <typename value_type, typename allocation_type>
typename vector<value_type, allocation_type>::iterator
vector<value_type, allocation_type>::insert(iterator const pos,
                                            const_reference value) {
  size_type position = pos - container_;
  if (c_size_ + 1 >= c_capacity_) {
    reserve(c_capacity_ * 2);
  }
  ++c_size_;
  value_type temporary = container_[position];
  container_[position] = value;
  for (size_type i = position + 1; i < c_size_; ++i) {
    value_type temp_save = container_[i];
    container_[i] = temporary;
    temporary = temp_save;
  }

  return container_ + position;
}

template <typename value_type, typename allocation_type>
void vector<value_type, allocation_type>::erase(iterator const pos) {
  size_type position = pos - container_;
  if (position > c_capacity_) {
    throw std::out_of_range("pos outside range");
  }

  iterator temporary = c_allocation_.allocate(c_size_ - 1);
  for (size_type i = 0, j = 0; i < c_size_; ++i) {
    if (i != position) {
      temporary[j] = container_[i];
      j++;
    }
  }
  c_allocation_.deallocate(container_, c_capacity_);
  container_ = temporary;
  --c_size_;
}

template <typename value_type, typename allocation_type>
void vector<value_type, allocation_type>::push_back(const_reference value) {
  if (c_size_ == c_capacity_) {
    size_type new_size;
    if (c_capacity_ == 0) {
      new_size = 1;
    } else {
      new_size = c_capacity_ * 2;
    }
    reserve(new_size);
  }
  c_allocation_.construct(container_ + c_size_++, value);
}

template <typename value_type, typename allocation_type>
void vector<value_type, allocation_type>::pop_back() {
  --c_size_;
  c_allocation_.destroy(container_ + c_size_);
}

template <typename value_type, typename allocation_type>
void vector<value_type, allocation_type>::swap(vector<value_type> &other) {
  std::swap(container_, other.container_);
  std::swap(c_capacity_, other.c_capacity_);
  std::swap(c_size_, other.c_size_);
}

template <typename value_type, typename allocator_type>
template <typename... Args>
typename vector<value_type, allocator_type>::iterator
vector<value_type, allocator_type>::emplace(const_iterator pos,
                                            Args &&...args) {
  iterator ret_pos = iterator(pos);
  const size_type size{sizeof...(args)};
  value_type temporary[size] = {args...};
  for (size_t i = 0; i < size; ++i) {
    ret_pos = insert(ret_pos, temporary[i]);
  }
  return ret_pos;
}

template <typename value_type, typename allocator_type>
template <typename... Args>
void vector<value_type, allocator_type>::emplace_back(Args &&...args) {
  value_type temporary[] = {args...};
  for (auto element : temporary) {
    push_back(element);
  }
}
