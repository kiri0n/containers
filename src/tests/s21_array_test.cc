#include "../s21_array.h"

#include <gtest/gtest.h>

#include <initializer_list>
#include <iostream>
#include <list>
using std::cout;
using std::endl;

TEST(array, create_default) {  // OK!
  s21::array<int, 0> s21_empty;
  std::array<int, 0> std_empty;
  EXPECT_EQ(s21_empty.size(), std_empty.size());
  EXPECT_EQ(s21_empty.empty(), std_empty.empty());

  s21::array<int, 3> s21_not_empty;
  std::array<int, 3> std_not_empty;
  EXPECT_EQ(s21_not_empty.size(), std_not_empty.size());
  EXPECT_EQ(s21_not_empty.empty(), std_not_empty.empty());

  s21::array<int, 3> s21_not_empty_2{2, 4, 6};
  std::array<int, 3> std_not_empty_2{2, 4, 6};
  EXPECT_EQ(s21_not_empty_2.size(), std_not_empty_2.size());
  EXPECT_EQ(s21_not_empty_2.empty(), std_not_empty_2.empty());
  for (size_t i = 0; i < 3; i++) {
    EXPECT_EQ(s21_not_empty_2.at(i), std_not_empty_2.at(i));
  }

  s21::array<double, 4> s21_empty_double;
  std::array<double, 4> std_empty_double;
  EXPECT_EQ(s21_empty_double.size(), std_empty_double.size());
  EXPECT_EQ(s21_empty_double.empty(), std_empty_double.empty());

  s21::array<double, 3> s21_not_empty_double_2{2.3, 4.2, 6.91};
  std::array<double, 3> std_not_empty_double_2{2.3, 4.2, 6.91};
  EXPECT_EQ(s21_not_empty_double_2.size(), std_not_empty_double_2.size());
  EXPECT_EQ(s21_not_empty_double_2.empty(), std_not_empty_double_2.empty());
  for (size_t i = 0; i < 3; i++) {
    EXPECT_EQ(s21_not_empty_double_2.at(i), std_not_empty_double_2.at(i));
  }

  s21::array<char, 5> s21_empty_char;
  std::array<char, 5> std_empty_char;
  EXPECT_EQ(s21_empty_char.size(), std_empty_char.size());
  EXPECT_EQ(s21_empty_char.empty(), std_empty_char.empty());

  s21::array<char, 3> s21_not_empty_char_2{'q', 'w', 'e'};
  std::array<char, 3> std_not_empty_char_2{'q', 'w', 'e'};
  EXPECT_EQ(s21_not_empty_char_2.size(), std_not_empty_char_2.size());
  EXPECT_EQ(s21_not_empty_char_2.empty(), std_not_empty_char_2.empty());
  for (size_t i = 0; i < 3; i++) {
    EXPECT_EQ(s21_not_empty_char_2.at(i), std_not_empty_char_2.at(i));
  }
}

TEST(array, copy) {  // OK!
  const size_t size = 3;
  s21::array<int, size> s21_int{2, 4, 6};
  s21::array<int, size> s21_int_copy(s21_int);

  std::array<int, size> std_int{2, 4, 6};
  std::array<int, size> std_int_copy(std_int);

  EXPECT_EQ(s21_int_copy.size(), std_int_copy.size());

  for (size_t i = 0; i < size; i++) {
    EXPECT_EQ(s21_int_copy.at(i), std_int_copy.at(i));
  }

  s21::array<char, size> s21_char{'A', '@', '%'};
  s21::array<char, size> s21_char_copy(s21_char);

  std::array<char, size> std_char{'A', '@', '%'};
  std::array<char, size> std_char_copy(std_char);

  EXPECT_EQ(s21_char_copy.size(), std_char_copy.size());

  for (size_t i = 0; i < size; i++) {
    EXPECT_EQ(s21_char_copy.at(i), std_char_copy.at(i));
  }

  s21::array<int, size> s21_int_empty;
  s21::array<int, size> s21_int_copy_empty(s21_int);

  std::array<int, size> std_int_empty;
  std::array<int, size> std_int_copy_empty(std_int_empty);

  EXPECT_EQ(s21_int_copy_empty.size(), std_int_copy_empty.size());
  EXPECT_EQ(s21_int_copy_empty.empty(), std_int_copy_empty.empty());
}

TEST(array, move) {  // OK!
  const size_t size = 3;
  s21::array<int, size> s21_int{2, 4, 6};
  s21::array<int, size> s21_int_move(std::move(s21_int));

  std::array<int, size> std_int{2, 4, 6};
  std::array<int, size> std_int_move(std::move(std_int));

  EXPECT_EQ(s21_int_move.size(), std_int_move.size());

  for (size_t i = 0; i < size; i++) {
    EXPECT_EQ(s21_int_move.at(i), std_int_move.at(i));
  }

  s21::array<char, size> s21_char{'A', '@', '%'};
  s21::array<char, size> s21_char_move(std::move(s21_char));

  std::array<char, size> std_char{'A', '@', '%'};
  std::array<char, size> std_char_move(std::move(std_char));

  EXPECT_EQ(s21_char_move.size(), std_char_move.size());

  for (size_t i = 0; i < size; i++) {
    EXPECT_EQ(s21_char_move.at(i), std_char_move.at(i));
  }

  s21::array<int, size> s21_int_empty;
  s21::array<int, size> s21_int_move_empty(std::move(s21_int_empty));

  std::array<int, size> std_int_empty;
  std::array<int, size> std_int_move_empty(std::move(std_int_empty));

  EXPECT_EQ(s21_int_move_empty.size(), std_int_move_empty.size());
  EXPECT_EQ(s21_int_move_empty.empty(), std_int_move_empty.empty());
}

TEST(array, operator_copy_move) {
  const size_t size = 3;
  s21::array<int, size> s21_int{2, 4, 6};
  s21::array<int, size> s21_int_mv;
  s21_int_mv = std::move(s21_int);

  std::array<int, size> std_int{2, 4, 6};
  std::array<int, size> std_int_mv;
  std_int_mv = std::move(std_int);

  EXPECT_EQ(s21_int_mv.size(), std_int_mv.size());

  for (size_t i = 0; i < size; i++) {
    EXPECT_EQ(s21_int_mv.at(i), std_int_mv.at(i));
  }

  EXPECT_EQ(s21_int.size(), std_int.size());
  EXPECT_EQ(s21_int.empty(), std_int.empty());

  s21::array<char, size> s21_char{'A', '@', '%'};
  s21::array<char, size> s21_char_eq;
  s21_char_eq = s21_char;

  std::array<char, size> std_char{'A', '@', '%'};
  std::array<char, size> std_char_eq;
  std_char_eq = std_char;

  EXPECT_EQ(s21_char_eq.size(), std_char_eq.size());

  for (size_t i = 0; i < size; i++) {
    EXPECT_EQ(s21_char_eq.at(i), std_char_eq.at(i));
  }
}

TEST(array, reference_at) {
  const size_t size = 6;
  s21::array<int, size> s21_int_at{2, 4, 6, 11, 44, 22};
  std::array<int, size> std_int_at{2, 4, 6, 11, 44, 22};

  for (size_t i = 0; i < size; i++) {
    EXPECT_EQ(s21_int_at.at(i), std_int_at.at(i));
  }
}

TEST(array, reference_operator_square) {
  const size_t size = 6;
  s21::array<int, size> s21_int_sq{2, 4, 6, 11, 44, 22};
  std::array<int, size> std_int_sq{2, 4, 6, 11, 44, 22};

  for (size_t i = 0; i < size; i++) {
    EXPECT_EQ(s21_int_sq[i], std_int_sq[i]);
  }
}

TEST(array, const_reference_front) {
  const size_t size = 6;
  s21::array<int, size> s21_int_front{2, 4, 6, 11, 44, 22};
  std::array<int, size> std_int_front{2, 4, 6, 11, 44, 22};

  EXPECT_EQ(s21_int_front.front(), std_int_front.front());
}

TEST(array, const_reference_back) {
  const size_t size = 6;
  s21::array<int, size> s21_int_back{2, 4, 6, 11, 44, 22};
  std::array<int, size> std_int_back{2, 4, 6, 11, 44, 22};

  EXPECT_EQ(s21_int_back.back(), std_int_back.back());
}

TEST(array, iterator_data) {
  const size_t size = 6;
  s21::array<int, size> s21_int_data{2, 4, 6, 11, 44, 22};
  std::array<int, size> std_int_data{2, 4, 6, 11, 44, 22};
  EXPECT_EQ(s21_int_data.data(), &s21_int_data.at(0));
  EXPECT_EQ(std_int_data.data(), &std_int_data.at(0));
}

TEST(array, iterator_begin) {
  s21::array<int, 3> s21_it_begin{2, 4, 6};
  std::array<int, 3> std_it_begin{2, 4, 6};
  EXPECT_EQ(s21_it_begin.begin(), &s21_it_begin[0]);
  EXPECT_EQ(std_it_begin.begin(), &std_it_begin[0]);
}

TEST(array, iterator_begin2) {
  s21::array<int, 3> s21_it_begin{10, 4, 6};
  int *iter1 = s21_it_begin.begin();

  std::array<int, 3> std_it_begin{10, 4, 6};
  int *iter2 = std_it_begin.begin();
  EXPECT_EQ(*iter1, *iter2);
}

TEST(array, iterator_end) {
  s21::array<int, 3> s21_it_end{2, 4, 6};
  std::array<int, 3> std_it_end{2, 4, 6};

  EXPECT_EQ(s21_it_end.end(), &s21_it_end[3]);
  EXPECT_EQ(std_it_end.end(), &std_it_end[3]);
}

TEST(array, iterator_end2) {
  s21::array<int, 4> s21_it_end{10, 4, 6, 12};
  int *iter_s21_e = s21_it_end.end();
  int *iter_s21_b = s21_it_end.begin();

  std::array<int, 4> std_it_end{10, 4, 6, 12};
  int *iter_or_e = std_it_end.end();
  int *iter_or_b = std_it_end.begin();

  while (iter_or_b < iter_or_e && iter_s21_b < iter_s21_e) {
    EXPECT_EQ(*iter_or_b, *iter_s21_b);
    iter_or_b++;
    iter_s21_b++;
  }
}

TEST(array, const_iterator_begin) {
  s21::array<int, 3> s21_int_const_beggin{2, 4, 6};
  EXPECT_EQ(s21_int_const_beggin.const_begin(), &s21_int_const_beggin[0]);
  const int *iter1 = s21_int_const_beggin.const_begin();

  std::array<int, 3> std_int_const_beggin{2, 4, 6};
  const int *iter2 = std_int_const_beggin.cbegin();
  EXPECT_EQ(*iter1, *iter2);
}

TEST(array, const_iterator_end) {
  s21::array<int, 4> s21_it_end{10, 4, 6, 12};
  const int *iter_s21_e = s21_it_end.const_end();
  const int *iter_s21_b = s21_it_end.const_begin();

  std::array<int, 4> std_it_end{10, 4, 6, 12};
  const int *iter_or_e = std_it_end.cend();
  const int *iter_or_b = std_it_end.cbegin();

  while (iter_or_b < iter_or_e && iter_s21_b < iter_s21_e) {
    EXPECT_EQ(*iter_or_b, *iter_s21_b);
    iter_or_b++;
    iter_s21_b++;
  }
}

TEST(array, empty) {
  s21::array<int, 0> s21_int_empty;
  std::array<int, 0> std_int_empty;
  EXPECT_EQ(s21_int_empty.empty(), std_int_empty.empty());

  s21::array<int, 2> s21_int;
  std::array<int, 2> std_int;
  EXPECT_EQ(s21_int.empty(), std_int.empty());

  s21::array<double, 0> s21_double_empty;
  std::array<double, 0> std_double_empty;
  EXPECT_EQ(s21_double_empty.empty(), std_double_empty.empty());

  s21::array<double, 2> s21_double;
  std::array<double, 2> std_double;
  EXPECT_EQ(s21_double.empty(), std_double.empty());

  s21::array<char, 0> s21_char_empty;
  std::array<char, 0> std_char_empty;
  EXPECT_EQ(s21_char_empty.empty(), std_char_empty.empty());

  s21::array<char, 2> s21_char;
  std::array<char, 2> std_char;
  EXPECT_EQ(s21_char.empty(), std_char.empty());
}

TEST(array, size) {
  s21::array<int, 5> s21_size{1, 4, 5, 7, 10};
  std::array<int, 5> std_size{1, 4, 5, 7, 10};
  EXPECT_EQ(s21_size.size(), std_size.size());

  s21::array<double, 5> s21_size_double;
  std::array<double, 5> std_size_double;
  EXPECT_EQ(s21_size_double.size(), std_size_double.size());
}

TEST(array, max_size) {
  s21::array<int, 5> s21_size{1, 4, 5, 7, 10};
  std::array<int, 5> std_size{1, 4, 5, 7, 10};
  EXPECT_EQ(s21_size.max_size(), std_size.max_size());

  s21::array<double, 5> s21_size_double;
  std::array<double, 5> std_size_double;
  EXPECT_EQ(s21_size_double.max_size(), std_size_double.max_size());
}

TEST(array, swap) {
  s21::array<int, 5> s21_int_swap{1, 2, 3};
  std::array<int, 5> std_int_swap{1, 2, 3};

  s21_int_swap.swap(s21_int_swap);
  std_int_swap.swap(std_int_swap);

  s21::array<int, 5> s21_swap{0, 0, 0};
  std::array<int, 5> std_swap{0, 0, 0};

  s21_int_swap.swap(s21_swap);
  std_int_swap.swap(std_swap);

  for (size_t i = 0; i < 3; i++) {
    EXPECT_EQ(s21_int_swap.at(i), std_int_swap.at(i));
  }

  for (size_t i = 0; i < 3; i++) {
    EXPECT_EQ(s21_swap.at(i), std_swap.at(i));
  }
}

TEST(array, something_else) {
  s21::array<int, 2> A;
  s21::array<int, 2> B(A);

  std::vector<int> X;
  std::vector<int> C(X);
}
