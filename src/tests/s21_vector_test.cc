#include "../s21_vector.h"

#include <gtest/gtest.h>

#include <initializer_list>
#include <iostream>
#include <vector>
using std::cout;
using std::endl;

TEST(vector, create_default) {
  s21::vector<int> s21_empty_vector;
  std::vector<int> std_empty_vector;
  EXPECT_EQ(s21_empty_vector.size(), std_empty_vector.size());
  EXPECT_EQ(s21_empty_vector.empty(), std_empty_vector.empty());

  s21::vector<int> s21_int_vector{1, 4, 10};
  std::vector<int> std_int_vector{1, 4, 10};
  EXPECT_EQ(s21_int_vector.size(), std_int_vector.size());
  EXPECT_EQ(s21_int_vector.empty(), std_int_vector.empty());

  for (size_t i = 0; i < 3; i++) {
    EXPECT_EQ(s21_int_vector.at(i), std_int_vector.at(i));
  }

  s21::vector<double> s21_double_vector{1.2, -4.291, 10.12};
  std::vector<double> std_double_vector{1.2, -4.291, 10.12};
  EXPECT_EQ(s21_double_vector.size(), std_double_vector.size());
  EXPECT_EQ(s21_double_vector.empty(), std_double_vector.empty());

  for (size_t i = 0; i < 3; i++) {
    EXPECT_EQ(s21_double_vector.at(i), std_double_vector.at(i));
  }

  s21::vector<char> s21_char_vector{'D', 'x', '*', '&'};
  std::vector<char> std_char_vector{'D', 'x', '*', '&'};
  EXPECT_EQ(s21_char_vector.size(), std_char_vector.size());
  EXPECT_EQ(s21_char_vector.empty(), std_char_vector.empty());

  for (size_t i = 0; i < 4; i++) {
    EXPECT_EQ(s21_char_vector.at(i), std_char_vector.at(i));
  }
}

TEST(vector, create_size) {
  std::vector<int> A(5);
  s21::vector<int> B(5);

  EXPECT_EQ(A.empty(), B.empty());
  EXPECT_EQ(A.size(), B.size());

  s21::vector<int> s21_int_size(5);
  std::vector<int> std_int_size(5);

  EXPECT_EQ(s21_int_size.size(), std_int_size.size());
  EXPECT_EQ(s21_int_size.empty(), std_int_size.empty());
}

TEST(vector, copy) {
  s21::vector<int> empty;
  s21::vector<int> empty_2(empty);
  std::vector<int> std_empty;
  std::vector<int> std_empty_2(std_empty);

  EXPECT_EQ(empty_2.size(), std_empty_2.size());
  EXPECT_EQ(empty_2.empty(), std_empty_2.empty());

  s21::vector<int> A{4, 5, 9, 10};
  s21::vector<int> B(A);
  std::vector<int> C{4, 5, 9, 10};

  EXPECT_EQ(B.size(), C.size());
  EXPECT_EQ(B.empty(), C.empty());

  for (size_t i = 0; i < A.size(); i++) {
    EXPECT_EQ(B.at(i), C.at(i));
  }

  s21::vector<double> s21_double{2.2, 4.1, 7.8};
  s21::vector<double> s21_move_double = s21_double;

  std::vector<double> std_double{2.2, 4.1, 7.8};
  std::vector<double> std_move_double = std_double;

  EXPECT_EQ(s21_double.empty(), std_double.empty());
  EXPECT_EQ(s21_double.size(), std_double.size());

  for (size_t i = 0; i < s21_double.size(); i++) {
    EXPECT_EQ(s21_move_double.at(i), std_move_double.at(i));
  }

  s21::vector<int> X = A;
  for (size_t i = 0; i < A.size(); i++) {
    EXPECT_EQ(A.at(i), X.at(i));
  }

  s21::vector<int> F;
  F = A;
  for (size_t i = 0; i < A.size(); i++) {
    EXPECT_EQ(A.at(i), F.at(i));
  }
  EXPECT_EQ(A.empty(), F.empty());
  EXPECT_EQ(A.size(), F.size());
}

TEST(vector, move) {
  s21::vector<int> empty;
  s21::vector<int> empty_2(std::move(empty));

  std::vector<int> std_empty;
  std::vector<int> std_empty_2(std::move(std_empty));

  EXPECT_EQ(empty_2.empty(), std_empty_2.empty());
  EXPECT_EQ(empty_2.size(), std_empty_2.size());

  s21::vector<int> s21{12, -9, 18, 49, 102};
  s21::vector<int> s21_2(std::move(s21));

  std::vector<int> std{12, -9, 18, 49, 102};
  std::vector<int> std_2(std::move(std));

  EXPECT_EQ(s21.empty(), std.empty());
  EXPECT_EQ(s21.size(), std.size());

  for (size_t i = 0; i < s21.size(); i++) {
    EXPECT_EQ(s21.at(i), std.at(i));
  }

  EXPECT_EQ(s21_2.empty(), std_2.empty());
  EXPECT_EQ(s21_2.size(), std_2.size());

  for (size_t i = 0; i < s21_2.size(); i++) {
    EXPECT_EQ(s21_2.at(i), std_2.at(i));
  }

  s21::vector<double> s21_double{2.2, 4.1, 7.8};
  s21::vector<double> s21_move_double;
  s21_move_double = std::move(s21_double);

  std::vector<double> std_double{2.2, 4.1, 7.8};
  std::vector<double> std_move_double;
  std_move_double = std::move(std_double);

  EXPECT_EQ(s21_move_double.empty(), std_move_double.empty());
  EXPECT_EQ(s21_move_double.size(), std_move_double.size());

  for (size_t i = 0; i < s21_double.size(); i++) {
    EXPECT_EQ(s21_move_double.at(i), std_move_double.at(i));
  }
}

TEST(vector, operatator_squre) {
  s21::vector<char> A{'x', '%', '*'};
  std::vector<char> B{'x', '%', '*'};
  for (size_t i = 0; i < A.size(); i++) {
    EXPECT_EQ(A[i], B[i]);
  }
}

TEST(vector, front_back) {
  s21::vector<int> A{1, 5, 10, 12};
  std::vector<int> B{1, 5, 10, 12};
  EXPECT_EQ(A.front(), B.front());
  EXPECT_EQ(A.back(), B.back());
}

TEST(vector, data) {
  std::vector<int> A{3, 12, 1, -1};
  s21::vector<int> B{3, 12, 1, -1};

  EXPECT_EQ(A.data(), &A.at(0));
  EXPECT_EQ(B.data(), &B.at(0));
  EXPECT_EQ(B.data() == nullptr, 0);
}

TEST(vector, begin_end) {
  s21::vector<int> A{1, 5, 10, 12};
  std::vector<int> B{1, 5, 10, 12};

  EXPECT_EQ(A.size(), B.size());
  EXPECT_EQ(A.capacity(), B.capacity());

  auto it1 = A.begin();
  auto it2 = B.begin();
  EXPECT_EQ(*it1, *it2);
}

TEST(vector, const_begin_end) {
  s21::vector<int> A{1, 5, 10, 12};
  std::vector<int> B{1, 5, 10, 12};
  EXPECT_EQ(A.size(), B.size());
  EXPECT_EQ(A.capacity(), B.capacity());

  auto it1 = A.const_begin();
  auto it2 = B.cbegin();
  EXPECT_EQ(*it1, *it2);
}

TEST(vector, reserve) {
  s21::vector<int> A{10, 2, -6, 1};
  A.reserve(10);

  std::vector<int> B{10, 2, -6, 1};
  B.reserve(10);

  EXPECT_EQ(A.size(), B.size());
  EXPECT_EQ(A.capacity(), B.capacity());
  EXPECT_EQ(A.max_size(), B.max_size());
}

TEST(vector, capacity) {
  s21::vector<int> A;
  std::vector<int> B;
  s21::vector<int> C{2, 10, 22, 33, 44};
  std::vector<int> D{2, 10, 22, 33, 44};

  EXPECT_EQ(A.size(), B.size());
  EXPECT_EQ(A.capacity(), B.capacity());
  EXPECT_EQ(C.size(), D.size());
  EXPECT_EQ(C.capacity(), D.capacity());
}

TEST(vector, shrink_to_fit) {
  s21::vector<int> A{10, 2, -6, 1};
  A.reserve(10);
  A.shrink_to_fit();

  std::vector<int> B{10, 2, -6, 1};
  B.reserve(10);
  B.shrink_to_fit();

  EXPECT_EQ(A.size(), B.size());
  EXPECT_EQ(A.capacity(), B.capacity());
  EXPECT_EQ(A.max_size(), B.max_size());
}

TEST(vector, clear) {
  s21::vector<int> A{10, 2, -6, 1};
  std::vector<int> B{10, 2, -6, 1};

  A.clear();
  B.clear();
  EXPECT_EQ(A.size(), B.size());
  EXPECT_EQ(A.capacity(), B.capacity());

  s21::vector<int> C;
  std::vector<int> D;

  C.clear();
  D.clear();

  EXPECT_EQ(C.size(), D.size());
  EXPECT_EQ(C.capacity(), D.capacity());
}

TEST(vector, insert) {
  s21::vector<int> s21_insert{1, 2, 3, 4, 5, 5, 4, 3, 2, 1};
  std::vector<int> std_insert{1, 2, 3, 4, 5, 5, 4, 3, 2, 1};

  auto it1 = s21_insert.end();
  auto it2 = std_insert.end();

  s21_insert.insert(it1, 111);
  std_insert.insert(it2, 111);

  for (size_t i = 0; i < s21_insert.size(); i++) {
    EXPECT_EQ(s21_insert.at(i), std_insert.at(i));
  }

  EXPECT_EQ(s21_insert.size(), std_insert.size());
  EXPECT_EQ(s21_insert.capacity(), std_insert.capacity());

  it1 = s21_insert.begin();
  it2 = std_insert.begin();

  s21_insert.insert(it1 + 5, 222);
  std_insert.insert(it2 + 5, 222);

  for (size_t i = 0; i < s21_insert.size(); i++) {
    EXPECT_EQ(s21_insert.at(i), std_insert.at(i));
  }
  EXPECT_EQ(s21_insert[5], 222);
  EXPECT_EQ(s21_insert.size(), std_insert.size());
  EXPECT_EQ(s21_insert.capacity(), std_insert.capacity());

  s21::vector<int> s21_supainsert;
  std::vector<int> std_supinsert;

  s21_supainsert.reserve(5);
  std_supinsert.reserve(5);

  it1 = s21_supainsert.begin();
  it2 = std_supinsert.begin();

  s21_supainsert.insert(it1 + 2, 12);

  EXPECT_EQ(s21_supainsert[2], 12);
  EXPECT_EQ(s21_supainsert.size(), 1);
  EXPECT_EQ(s21_supainsert.capacity(), 5);
}

TEST(vector, erase) {
  s21::vector<int> A{1, 2, 3, 4, 22};
  std::vector<int> B{1, 2, 3, 4, 22};

  EXPECT_EQ(A.size(), 5);
  EXPECT_EQ(A.capacity(), 5);
  auto it = A.begin();
  auto it2 = B.begin();

  EXPECT_EQ(A.size(), B.size());
  EXPECT_EQ(A.capacity(), B.capacity());

  A.erase(it + 2);
  B.erase(it2 + 2);

  EXPECT_EQ(A.size(), B.size());
  EXPECT_EQ(A.capacity(), B.capacity());

  for (size_t i = 0; i < A.size(); i++) {
    EXPECT_EQ(A.at(i), B.at(i));
  }

  auto it3 = A.end();
  auto it4 = B.end();
  EXPECT_EQ(A.size(), B.size());
  EXPECT_EQ(A.capacity(), B.capacity());

  A.erase(it3 - 1);
  B.erase(it4 - 1);
  EXPECT_EQ(A.size(), B.size());
  EXPECT_EQ(A.capacity(), B.capacity());

  for (size_t i = 0; i < A.size(); i++) {
    EXPECT_EQ(A.at(i), B.at(i));
  }
}

TEST(vector, push_back) {
  s21::vector<int> A{22, 12, 10, -10};
  std::vector<int> B{22, 12, 10, -10};

  A.push_back(-22);
  B.push_back(-22);

  for (size_t i = 0; i < A.size(); i++) {
    EXPECT_EQ(A.at(i), B.at(i));
  }

  EXPECT_EQ(A.size(), B.size());
  EXPECT_EQ(A.capacity(), B.capacity());
}

TEST(vector, pop_back) {
  s21::vector<int> A{11, 99, 10, 98};
  std::vector<int> B{11, 99, 10, 98};

  A.pop_back();
  B.pop_back();

  EXPECT_EQ(A.size(), B.size());
  EXPECT_EQ(A.capacity(), B.capacity());

  s21::vector<int> C;
  std::vector<int> D;

  C.pop_back();
  D.pop_back();
  EXPECT_EQ(C.size(), D.size());
  EXPECT_EQ(C.capacity(), D.capacity());
}

TEST(vector, swap) {
  s21::vector<int> A{-9, -10, -11, -12};
  std::vector<int> B{-9, -10, -11, -12};
  s21::vector<int> C{1, 3, 5, 7};
  std::vector<int> D{1, 3, 5, 7};

  A.swap(C);
  B.swap(D);

  EXPECT_EQ(A.size(), A.size());
  EXPECT_EQ(A.capacity(), A.capacity());
  EXPECT_EQ(C.size(), D.size());
  EXPECT_EQ(C.capacity(), D.capacity());

  for (size_t i = 0; i < B.size(); i++) {
    EXPECT_EQ(A.at(i), B.at(i));
  }

  for (size_t i = 0; i < D.size(); i++) {
    EXPECT_EQ(C.at(i), D.at(i));
  }

  s21::vector<int> X{10, 12, 14};
  std::vector<int> Y{10, 12, 14};

  X.swap(X);
  Y.swap(Y);
}

TEST(vector, emplace) {
  std::vector<int> B{12, 17, 13, 44, 11};
  B.emplace(B.begin() + 2, 100);
  B.emplace(B.end() - 2, 120);

  s21::vector<int> A{12, 17, 13, 44, 11};
  A.emplace(A.begin() + 2, 100);
  A.emplace(A.end() - 2, 120);

  for (size_t i = 0; i < A.size(); i++) {
    EXPECT_EQ(A.at(i), B.at(i));
  }
}

TEST(vector, emplace_version_2) {
  std::vector<int> B{12, 17, 4, 3, 2, 13, 1, 44, 11};

  s21::vector<int> A{12, 17, 13, 44, 11};
  A.emplace(A.begin() + 2, 2, 3, 4);
  A.emplace(A.end() - 2, 1);

  for (size_t i = 0; i < A.size(); i++) {
    EXPECT_EQ(A.at(i), B.at(i));
  }
}

TEST(vector, emplace_back) {
  s21::vector<int> A{10, 20, 30};
  A.emplace_back(40);
  A.emplace_back(50);
  A.emplace_back(60);
  A.emplace_back(70);

  std::vector<int> B{10, 20, 30};
  B.emplace_back(40);
  B.emplace_back(50);
  B.emplace_back(60);
  B.emplace_back(70);

  for (size_t i = 0; i < A.size(); i++) {
    EXPECT_EQ(A.at(i), B.at(i));
  }
}

TEST(vector, emplace_back_version_2) {
  s21::vector<int> A{10, 20, 30};
  std::vector<int> B{10, 20, 30, 40, 50, 60, 70, 80, 90};
  A.emplace_back(40);
  A.emplace_back(50, 60);
  A.emplace_back(70);
  A.emplace_back(80, 90);
  for (size_t i = 0; i < A.size(); i++) {
    EXPECT_EQ(A.at(i), B.at(i));
  }
}

TEST(vector, iterator) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::vector<int> result(n);
  std::vector<int> expect(n);
  auto it = result.begin();
  auto it2 = expect.begin();
  for (; it != result.end() && it2 != expect.end(); ++it, ++it2) {
    ASSERT_EQ(*it, *it2);
  }
}
