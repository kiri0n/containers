#include "../s21_queue.h"

#include <gtest/gtest.h>

#include <initializer_list>
#include <iostream>
#include <queue>
using std::cout;
using std::endl;

void CompareQueueInt(s21::queue<int> result, std::queue<int> expect) {
  auto size_result = result.size();
  auto size_expect = expect.size();
  if (size_result == size_expect) {
    while (result.size()) {
      ASSERT_EQ(result.front(), expect.front());
      result.pop();
      expect.pop();
    }
  } else {
    cout << "expect size: " << expect.size()
         << "| result size: " << result.size() << endl;
    ADD_FAILURE();
  }
}

TEST(queue, constructor_default_1) {
  s21::queue<int> result;
  std::queue<int> expect;
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(queue, constructor_initlist_2) {
  std::initializer_list<int> n{};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(result, expect);
}

TEST(queue, constructor_initlist_3) {
  std::initializer_list<int> n{1};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(queue, constructor_initlist_4) {
  std::initializer_list<int> n{1, 2};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(queue, constructor_initlist_5) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(result, expect);
}

TEST(queue, constructor_copy_6) {
  std::initializer_list<int> n{};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  s21::queue<int> result(resultStart);
  std::queue<int> expect(expectStart);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(queue, constructor_copy_7) {
  std::initializer_list<int> n{1};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  s21::queue<int> result(resultStart);
  std::queue<int> expect(expectStart);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(queue, constructor_copy_8) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  s21::queue<int> result(resultStart);
  std::queue<int> expect(expectStart);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
  ASSERT_EQ(result.front(), expect.front());
  ASSERT_EQ(result.back(), expect.back());
}

TEST(queue, constructor_move_9) {
  std::initializer_list<int> n{};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  s21::queue<int> result(std::move(resultStart));
  std::queue<int> expect(std::move(expectStart));
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(queue, constructor_move_10) {
  std::initializer_list<int> n{1};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  s21::queue<int> result(std::move(resultStart));
  std::queue<int> expect(std::move(expectStart));
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(queue, constructor_move_11) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  s21::queue<int> result(std::move(resultStart));
  std::queue<int> expect(std::move(expectStart));
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(queue, operator_move_1) {
  std::initializer_list<int> n{};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  s21::queue<int> result = std::move(resultStart);
  std::queue<int> expect = std::move(expectStart);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(queue, operator_move_2) {
  std::initializer_list<int> n{1};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  s21::queue<int> result = std::move(resultStart);
  std::queue<int> expect = std::move(expectStart);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(queue, operator_move_4) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  std::initializer_list<int> n1{1, 2, 3, 4, 5};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  s21::queue<int> result(n1);
  std::queue<int> expect(n1);
  result = std::move(resultStart);
  expect = std::move(expectStart);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(queue, operator_move_3) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  s21::queue<int> result;
  std::queue<int> expect;
  result = std::move(resultStart);
  expect = std::move(expectStart);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(queue, front_back_1) {
  std::initializer_list<int> n{1};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  ASSERT_EQ(resultStart.front(), expectStart.front());
  ASSERT_EQ(resultStart.back(), expectStart.back());
}

TEST(queue, front_back_2) {
  std::initializer_list<int> n{1, 2, 3, 4, 5};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  ASSERT_EQ(resultStart.front(), expectStart.front());
  ASSERT_EQ(resultStart.back(), expectStart.back());
}

TEST(queue, empty_1) {
  s21::queue<int> resultStart;
  std::queue<int> expectStart;
  ASSERT_TRUE((resultStart.empty() == expectStart.empty()));
}

TEST(queue, empty_2) {
  std::initializer_list<int> n{4};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
}

TEST(queue, empty_3) {
  std::initializer_list<int> n{1, 2, 3, 4, 5};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
}

TEST(queue, size_1) {
  s21::queue<int> resultStart;
  std::queue<int> expectStart;
  ASSERT_TRUE((resultStart.size() == expectStart.size()));
}

TEST(queue, size_2) {
  std::initializer_list<int> n{4};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(queue, size_3) {
  std::initializer_list<int> n{1, 2, 3, 4, 5};
  s21::queue<int> resultStart(n);
  std::queue<int> expectStart(n);
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(queue, push_1) {
  s21::queue<int> result;
  std::queue<int> expect;
  int value{10};
  int &val = value;
  result.push(val);
  expect.push(val);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(queue, push_2) {
  std::initializer_list<int> n{1};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  int value{10};
  int &val = value;
  result.push(val);
  expect.push(val);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(queue, push_3) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  int value{10};
  int &val = value;
  result.push(val);
  expect.push(val);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(queue, pop_1) {
  std::initializer_list<int> n{1};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  result.pop();
  expect.pop();
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(queue, pop_2) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  result.pop();
  expect.pop();
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(queue, swap_1) {
  std::initializer_list<int> n{};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  s21::queue<int> result1(n);
  std::queue<int> expect1(n);
  result.swap(result1);
  expect.swap(expect1);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(queue, swap_2) {
  std::initializer_list<int> n{};
  std::initializer_list<int> m{5};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  s21::queue<int> result1(m);
  std::queue<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(queue, swap_3) {
  std::initializer_list<int> n{5};
  std::initializer_list<int> m{};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  s21::queue<int> result1(m);
  std::queue<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(queue, swap_4) {
  std::initializer_list<int> n{1};
  std::initializer_list<int> m{5};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  s21::queue<int> result1(m);
  std::queue<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(queue, swap_5) {
  std::initializer_list<int> n{};
  std::initializer_list<int> m{5, 6, 7, 8, 9};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  s21::queue<int> result1(m);
  std::queue<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(queue, swap_6) {
  std::initializer_list<int> n{5, 6, 7, 8, 9};
  std::initializer_list<int> m{};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  s21::queue<int> result1(m);
  std::queue<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(queue, swap_7) {
  std::initializer_list<int> n{1, 2};
  std::initializer_list<int> m{5, 6, 7, 8, 9};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  s21::queue<int> result1(m);
  std::queue<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(queue, swap_8) {
  std::initializer_list<int> m{1, 2};
  std::initializer_list<int> n{5, 6, 7, 8, 9};
  s21::queue<int> result(n);
  std::queue<int> expect(n);
  s21::queue<int> result1(m);
  std::queue<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareQueueInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareQueueInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(queue, emplace_back_1) {
  std::initializer_list<int> n{1, 2};
  s21::queue<int> result(n);
  result.emplace_back(3, 4, 5, 6, 7);
  ASSERT_EQ(result.size(), 7);
  ASSERT_EQ(result.front(), 1);
  ASSERT_EQ(result.back(), 7);
}

TEST(queue, emplace_back_2) {
  s21::queue<int> result;
  ASSERT_EQ(result.size(), 0);
  result.emplace_back(3, 4, 5, 6, 7);
  ASSERT_EQ(result.size(), 5);
  ASSERT_EQ(result.front(), 3);
  ASSERT_EQ(result.back(), 7);
  result.pop();
  result.pop();
  ASSERT_EQ(result.size(), 3);
  ASSERT_EQ(result.front(), 5);
  ASSERT_EQ(result.back(), 7);
  result.emplace_back(8, 9, 10, 11, 12, 13, 14, 15, 16);
  ASSERT_EQ(result.size(), 12);
  ASSERT_EQ(result.front(), 5);
  ASSERT_EQ(result.back(), 16);
}
