#include "../s21_list.h"

#include <gtest/gtest.h>

#include <initializer_list>
#include <iostream>
#include <list>
using std::cout;
using std::endl;

void CompareListInt(s21::list<int> const &result,
                    std::list<int> const &expect) {
  auto i_result = result.begin();
  auto i_expect = expect.begin();
  while (i_result != result.end() && i_expect != expect.end()) {
    if (*i_result != *i_expect) {
      break;
    }
    ++i_result;
    ++i_expect;
  }
  if (i_result != result.end() || i_expect != expect.end()) {
    ADD_FAILURE();
  }
}

TEST(list, constructor_default_1) {
  s21::list<int> result;
  std::list<int> expect;
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
}

TEST(list, constructor_size_2) {
  s21::list<int> result(0);
  std::list<int> expect(0);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
}

TEST(list, constructor_size_3) {
  s21::list<int> result(1);
  std::list<int> expect(1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
}

TEST(list, constructor_size_4) {
  size_t n{5};
  s21::list<int> result(n);
  std::list<int> expect(n);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
}

TEST(list, constructor_size__5) {
  s21::list<int> result(12);
  std::list<int> expect(12);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, constructor_initlist_6) {
  std::initializer_list<int> n{};
  s21::list<int> result(n);
  std::list<int> expect(n);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
}

TEST(list, constructor_initlist_7) {
  std::initializer_list<int> n{1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
}

TEST(list, constructor_initlist_8) {
  std::initializer_list<int> n{1, 2};
  s21::list<int> result(n);
  std::list<int> expect(n);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
}

TEST(list, constructor_initlist__9) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
}

TEST(list, constructor_copy_10) {
  std::initializer_list<int> n{};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  s21::list<int> result(resultStart);
  std::list<int> expect(expectStart);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
  CompareListInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
  ASSERT_EQ(resultStart.max_size(), expectStart.max_size());
}

TEST(list, constructor_copy_11) {
  std::initializer_list<int> n{1};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  s21::list<int> result(resultStart);
  std::list<int> expect(expectStart);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
  CompareListInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
  ASSERT_EQ(resultStart.max_size(), expectStart.max_size());
}

TEST(list, constructor_copy_12) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  s21::list<int> result(resultStart);
  std::list<int> expect(expectStart);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
  CompareListInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
  ASSERT_EQ(resultStart.max_size(), expectStart.max_size());
}

TEST(list, constructor_move_13) {
  std::initializer_list<int> n{};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  s21::list<int> result(std::move(resultStart));
  std::list<int> expect(std::move(expectStart));
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
  CompareListInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
  ASSERT_EQ(resultStart.max_size(), expectStart.max_size());
}

TEST(list, constructor_move_14) {
  std::initializer_list<int> n{1};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  s21::list<int> result(std::move(resultStart));
  std::list<int> expect(std::move(expectStart));
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
  CompareListInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
  ASSERT_EQ(resultStart.max_size(), expectStart.max_size());
}

TEST(list, constructor_move_15) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  s21::list<int> result(std::move(resultStart));
  std::list<int> expect(std::move(expectStart));
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
  CompareListInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
  ASSERT_EQ(resultStart.max_size(), expectStart.max_size());
}

TEST(list, operator_move_1) {
  std::initializer_list<int> n{};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  s21::list<int> result = std::move(resultStart);
  std::list<int> expect = std::move(expectStart);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
  CompareListInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
  ASSERT_EQ(resultStart.max_size(), expectStart.max_size());
}

TEST(list, operator_move_2) {
  std::initializer_list<int> n{1};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  s21::list<int> result = std::move(resultStart);
  std::list<int> expect = std::move(expectStart);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
  CompareListInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
  ASSERT_EQ(resultStart.max_size(), expectStart.max_size());
}

TEST(list, operator_move_3) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  s21::list<int> result = std::move(resultStart);
  std::list<int> expect = std::move(expectStart);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.max_size(), expect.max_size());
  CompareListInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
  ASSERT_EQ(resultStart.max_size(), expectStart.max_size());
}

TEST(list, front_back_1) {
  std::initializer_list<int> n{1};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  ASSERT_EQ(resultStart.front(), expectStart.front());
  ASSERT_EQ(resultStart.back(), expectStart.back());
}

TEST(list, front_back_2) {
  std::initializer_list<int> n{1, 2, 3, 4, 5};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  ASSERT_EQ(resultStart.front(), expectStart.front());
  ASSERT_EQ(resultStart.back(), expectStart.back());
}

TEST(list, begin_end_1) {
  s21::list<int> resultStart;
  std::list<int> expectStart;
  ASSERT_TRUE((resultStart.begin() == resultStart.end()));
  ASSERT_TRUE((expectStart.begin() == expectStart.end()));
}

TEST(list, begin_end_2) {
  s21::list<int> resultStart(0);
  std::list<int> expectStart(0);
  ASSERT_TRUE((resultStart.begin() == resultStart.end()));
  ASSERT_TRUE((expectStart.begin() == expectStart.end()));
}

TEST(list, begin_end_3) {
  std::initializer_list<int> n{4};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  ASSERT_EQ(*resultStart.begin(), *expectStart.begin());
  auto resultEnd = resultStart.end();
  auto expectEnd = expectStart.end();
  --resultEnd, --expectEnd;
  ASSERT_EQ(*resultEnd, *expectEnd);
}

TEST(list, begin_end_4) {
  std::initializer_list<int> n{1, 2, 3, 4, 5};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  ASSERT_EQ(*resultStart.begin(), *expectStart.begin());
  auto resultEnd = resultStart.end();
  auto expectEnd = expectStart.end();
  --resultEnd, --expectEnd;
  ASSERT_EQ(*resultEnd, *expectEnd);
}

TEST(list, empty_1) {
  s21::list<int> resultStart;
  std::list<int> expectStart;
  ASSERT_TRUE((resultStart.empty() == expectStart.empty()));
}

TEST(list, empty_2) {
  s21::list<int> resultStart(0);
  std::list<int> expectStart(0);
  ASSERT_TRUE((resultStart.empty() == expectStart.empty()));
}

TEST(list, empty_3) {
  std::initializer_list<int> n{4};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
}

TEST(list, empty_4) {
  std::initializer_list<int> n{1, 2, 3, 4, 5};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
}

TEST(list, size_1) {
  s21::list<int> resultStart;
  std::list<int> expectStart;
  ASSERT_TRUE((resultStart.size() == expectStart.size()));
}

TEST(list, size_2) {
  s21::list<int> resultStart(0);
  std::list<int> expectStart(0);
  ASSERT_TRUE((resultStart.size() == expectStart.size()));
}

TEST(list, size_3) {
  std::initializer_list<int> n{4};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(list, size_4) {
  std::initializer_list<int> n{1, 2, 3, 4, 5};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(list, max_size_1) {
  s21::list<int> resultStart;
  std::list<int> expectStart;
  ASSERT_TRUE((resultStart.max_size() == expectStart.max_size()));
}

TEST(list, max_size_2) {
  s21::list<int> resultStart(0);
  std::list<int> expectStart(0);
  ASSERT_TRUE((resultStart.max_size() == expectStart.max_size()));
}

TEST(list, max_size_3) {
  std::initializer_list<int> n{4};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  ASSERT_EQ(resultStart.max_size(), expectStart.max_size());
}

TEST(list, max_size_4) {
  std::initializer_list<int> n{1, 2, 3, 4, 5};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  ASSERT_EQ(resultStart.max_size(), expectStart.max_size());
}

TEST(list, clear_1) {
  s21::list<int> resultStart;
  std::list<int> expectStart;
  resultStart.clear(), expectStart.clear();
  ASSERT_TRUE((resultStart.size() == expectStart.size()));
  ASSERT_TRUE((resultStart.begin() == resultStart.end()));
  ASSERT_TRUE((expectStart.begin() == expectStart.end()));
}

TEST(list, clear_2) {
  s21::list<int> resultStart(0);
  std::list<int> expectStart(0);
  resultStart.clear(), expectStart.clear();
  ASSERT_TRUE((resultStart.size() == expectStart.size()));
  ASSERT_TRUE((resultStart.begin() == resultStart.end()));
  ASSERT_TRUE((expectStart.begin() == expectStart.end()));
}

TEST(list, clear_3) {
  std::initializer_list<int> n{4};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  resultStart.clear(), expectStart.clear();
  ASSERT_EQ(resultStart.size(), expectStart.size());
  ASSERT_TRUE((resultStart.begin() == resultStart.end()));
  ASSERT_TRUE((expectStart.begin() == expectStart.end()));
}

TEST(list, clear_4) {
  std::initializer_list<int> n{1, 2, 3, 4, 5};
  s21::list<int> resultStart(n);
  std::list<int> expectStart(n);
  resultStart.clear(), expectStart.clear();
  ASSERT_EQ(resultStart.size(), expectStart.size());
  ASSERT_TRUE((resultStart.begin() == resultStart.end()));
  ASSERT_TRUE((expectStart.begin() == expectStart.end()));
}

TEST(list, push_back_1) {
  s21::list<int> result;
  std::list<int> expect;
  int value{10};
  int &val = value;
  result.push_back(val);
  expect.push_back(val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, push_back_2) {
  std::initializer_list<int> n{1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  int value{10};
  int &val = value;
  result.push_back(val);
  expect.push_back(val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, push_back_3) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  int value{10};
  int &val = value;
  result.push_back(val);
  expect.push_back(val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, swap_1) {
  std::initializer_list<int> n{};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(n);
  std::list<int> expect1(n);
  result.swap(result1);
  expect.swap(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, swap_2) {
  std::initializer_list<int> n{};
  std::initializer_list<int> m{5};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, swap_3) {
  std::initializer_list<int> n{5};
  std::initializer_list<int> m{};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, swap_4) {
  std::initializer_list<int> n{1};
  std::initializer_list<int> m{5};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, swap_5) {
  std::initializer_list<int> n{};
  std::initializer_list<int> m{5, 6, 7, 8, 9};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, swap_6) {
  std::initializer_list<int> n{5, 6, 7, 8, 9};
  std::initializer_list<int> m{};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, swap_7) {
  std::initializer_list<int> n{1, 2};
  std::initializer_list<int> m{5, 6, 7, 8, 9};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, swap_8) {
  std::initializer_list<int> m{1, 2};
  std::initializer_list<int> n{5, 6, 7, 8, 9};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, insert_1) {
  s21::list<int> result;
  std::list<int> expect;
  int value{10};
  int &val = value;
  auto posResult = result.begin();
  auto itResult = result.insert(posResult, val);
  auto itExpect = expect.insert(expect.begin(), val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(*itResult, *itExpect);
}

TEST(list, insert_2) {
  s21::list<int> result;
  std::list<int> expect;
  int value{10};
  int &val = value;
  auto posResult = result.end();
  auto itResult = result.insert(posResult, val);
  auto itExpect = expect.insert(expect.end(), val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(*itResult, *itExpect);
}

TEST(list, insert_3) {
  std::initializer_list<int> n{1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  int value{10};
  int &val = value;
  auto posResult = result.begin();
  auto itResult = result.insert(posResult, val);
  auto itExpect = expect.insert(expect.begin(), val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(*itResult, *itExpect);
}

TEST(list, insert_4) {
  std::initializer_list<int> n{1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  int value{10};
  int &val = value;
  auto posResult = result.end();
  auto itResult = result.insert(posResult, val);
  auto itExpect = expect.insert(expect.end(), val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(*itResult, *itExpect);
}

TEST(list, insert_5) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  int value{10};
  int &val = value;
  auto posResult = result.begin();
  auto itResult = result.insert(posResult, val);
  auto itExpect = expect.insert(expect.begin(), val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(*itResult, *itExpect);
}

TEST(list, insert_6) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  int value{10};
  int &val = value;
  auto posResult = result.end();
  auto itResult = result.insert(posResult, val);
  auto itExpect = expect.insert(expect.end(), val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(*itResult, *itExpect);
}

TEST(list, insert_7) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  int value{10};
  int &val = value;
  auto posResult = result.begin();
  ++posResult;
  ++posResult;
  auto itResult = result.insert(posResult, val);
  auto itExpect = expect.insert(++(++expect.begin()), val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(*itResult, *itExpect);
}

TEST(list, insert_8) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  int value{10};
  int &val = value;
  auto posResult = result.end();
  --posResult;
  --posResult;
  auto itResult = result.insert(posResult, val);
  auto itExpect = expect.insert(--(--expect.end()), val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(*itResult, *itExpect);
}

TEST(list, erase1_1) {
  std::initializer_list<int> n{1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  auto posResult = result.begin();
  result.erase(posResult);
  expect.erase(expect.begin());
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, erase_2) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  auto posResult = result.begin();
  result.erase(posResult);
  expect.erase(expect.begin());
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, erase_3) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  auto posResult = result.begin();
  ++posResult;
  ++posResult;
  result.erase(posResult);
  expect.erase(++(++expect.begin()));
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, erase_4) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  auto posResult = result.end();
  --posResult;
  --posResult;
  result.erase(posResult);
  expect.erase(--(--expect.end()));
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, push_front_1) {
  s21::list<int> result;
  std::list<int> expect;
  int value{10};
  int &val = value;
  result.push_front(val);
  expect.push_front(val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, push_front_2) {
  std::initializer_list<int> n{1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  int value{10};
  int &val = value;
  result.push_front(val);
  expect.push_front(val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, push_front_3) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  int value{10};
  int &val = value;
  result.push_front(val);
  expect.push_front(val);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, pop_front_1) {
  std::initializer_list<int> n{1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  result.pop_front();
  expect.pop_front();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, pop_front_2) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  result.pop_front();
  expect.pop_front();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, pop_back_1) {
  std::initializer_list<int> n{1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  result.pop_back();
  expect.pop_back();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, pop_back_2) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  result.pop_back();
  expect.pop_back();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, merge_1) {
  std::initializer_list<int> n{};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(n);
  std::list<int> expect1(n);
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, merge_2) {
  std::initializer_list<int> n{};
  std::initializer_list<int> m{5};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, merge_3) {
  std::initializer_list<int> n{5};
  std::initializer_list<int> m{};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, merge_4) {
  std::initializer_list<int> n{1};
  std::initializer_list<int> m{5};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, merge_5) {
  std::initializer_list<int> n{};
  std::initializer_list<int> m{5, 6, 7, 8, 9};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, merge_6) {
  std::initializer_list<int> n{5, 6, 7, 8, 9};
  std::initializer_list<int> m{};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, merge_7) {
  std::initializer_list<int> n{1, 2};
  std::initializer_list<int> m{5, 6, 7, 8, 9};
  s21::list<int> result(n);   // 1 2
  std::list<int> expect(n);   // 1 2
  s21::list<int> result1(m);  // 5 6 7 8 9
  std::list<int> expect1(m);  // 5 6 7 8 9
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, merge_8) {
  std::initializer_list<int> m{1, 2};
  std::initializer_list<int> n{5, 6, 7, 8, 9};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, merge_9) {
  std::initializer_list<int> n{1, 3, 5, 7, 9};
  std::initializer_list<int> m{2, 4, 6, 8, 10, 12};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, merge_10) {
  std::initializer_list<int> m{1, 3, 5, 7, 9};
  std::initializer_list<int> n{2, 4, 6, 8, 10, 12};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, merge_11) {
  std::initializer_list<int> n{1, 3, 3, 9, 9};
  std::initializer_list<int> m{2, 3, 6, 9, 10, 12};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, merge_12) {
  std::initializer_list<int> m{1, 3, 3, 9, 9};
  std::initializer_list<int> n{2, 3, 6, 9, 10, 12};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, merge_13) {
  std::initializer_list<int> n{1, 3, 8, 4, 9};
  std::initializer_list<int> m{2, 6, 10, 9, 3, 12};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, merge_14) {
  std::initializer_list<int> m{5, 3, 5, 9, 9};
  std::initializer_list<int> n{2, 3, 6, 3, 1, 12};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  result.merge(result1);
  expect.merge(expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareListInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(list, splice_1) {
  s21::list<int> result;
  std::list<int> expect;
  s21::list<int> result1;
  std::list<int> expect1;
  auto posResult = result.begin();
  result.splice(posResult, result1);
  expect.splice(expect.begin(), expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, splice_2) {
  s21::list<int> result;
  std::list<int> expect;
  s21::list<int> result1;
  std::list<int> expect1;
  auto posResult = result.end();
  result.splice(posResult, result1);
  expect.splice(expect.end(), expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, splice_3) {
  std::initializer_list<int> n{0};
  std::initializer_list<int> m{1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  auto posResult = result.begin();
  result.splice(posResult, result1);
  expect.splice(expect.begin(), expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, splice_4) {
  std::initializer_list<int> n{1};
  std::initializer_list<int> m{0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  auto posResult = result.begin();
  result.splice(posResult, result1);
  expect.splice(expect.begin(), expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, splice_5) {
  std::initializer_list<int> n{};
  std::initializer_list<int> m{1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  auto posResult = result.end();
  result.splice(posResult, result1);
  expect.splice(expect.end(), expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, splice_6) {
  std::initializer_list<int> n{1};
  std::initializer_list<int> m{0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  auto posResult = result.end();
  result.splice(posResult, result1);
  expect.splice(expect.end(), expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, splice_7) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  std::initializer_list<int> m{1, 7, 12};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  auto posResult = result.begin();
  result.splice(posResult, result1);
  expect.splice(expect.begin(), expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, splice_8) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  std::initializer_list<int> m{1, 7, 12};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  auto posResult = result.end();
  result.splice(posResult, result1);
  expect.splice(expect.end(), expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, splice_9) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  std::initializer_list<int> m{1, 7, 12};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  auto posResult = result.begin();
  ++posResult;
  ++posResult;
  result.splice(posResult, result1);
  expect.splice(++(++expect.begin()), expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, splice_10) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  std::initializer_list<int> m{1, 7, 12};
  s21::list<int> result(n);
  std::list<int> expect(n);
  s21::list<int> result1(m);
  std::list<int> expect1(m);
  auto posResult = result.end();
  --posResult;
  --posResult;
  result.splice(posResult, result1);
  expect.splice(--(--expect.end()), expect1);
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, reverse_1) {
  s21::list<int> result;
  std::list<int> expect;
  result.reverse();
  expect.reverse();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, reverse_2) {
  std::initializer_list<int> n{1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  result.reverse();
  expect.reverse();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, reverse_3) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::list<int> result(n);
  std::list<int> expect(n);
  result.reverse();
  expect.reverse();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, unique_1) {
  s21::list<int> result;
  std::list<int> expect;
  result.unique();
  expect.unique();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, unique_2) {
  std::initializer_list<int> n{1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  result.unique();
  expect.unique();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, unique_3) {
  std::initializer_list<int> n{1, 2, 3, 2, 5, 6, 2, 8, 8, 8};
  s21::list<int> result(n);
  std::list<int> expect(n);
  result.unique();
  expect.unique();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, unique_4) {
  std::initializer_list<int> n{1, 2, 2, 2, 5, 6, 2, 8, 8, 8};
  s21::list<int> result(n);
  std::list<int> expect(n);
  result.unique();
  expect.unique();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, sort_1) {
  s21::list<int> result;
  std::list<int> expect;
  result.sort();
  expect.sort();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, sort_2) {
  std::initializer_list<int> n{1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  result.sort();
  expect.sort();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, sort_3) {
  std::initializer_list<int> n{1, 2, 3, 2, 5, 6, 2, 8, 8, 8};
  s21::list<int> result(n);
  std::list<int> expect(n);
  result.sort();
  expect.sort();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, sort_4) {
  std::initializer_list<int> n{1, 2, 2, 2, 5, 6, 2, 8, 8, 8};
  s21::list<int> result(n);
  std::list<int> expect(n);
  result.sort();
  expect.sort();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, sort_5) {
  std::initializer_list<int> n{9, 2, 5, 3, 4, 8, 7, 6, 9, 1};
  s21::list<int> result(n);
  std::list<int> expect(n);
  result.sort();
  expect.sort();
  CompareListInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(list, emplace_1) {
  s21::list<int> result;
  auto it = result.begin();
  result.emplace(it, 1, 2, 3, 4, 5, 6, 7, 8, 9);
  int i = 9;
  for (it = result.begin(); it != result.end(); ++it) {
    ASSERT_EQ(*it, i);
    --i;
  }
}

TEST(list, emplace_back_1) {
  s21::list<int> result;
  result.emplace_back(1, 2, 3, 4, 5, 6, 7, 8, 9);
  int i = 1;
  for (auto it = result.begin(); it != result.end(); ++it) {
    ASSERT_EQ(*it, i);
    ++i;
  }
}

TEST(list, emplace_front_1) {
  s21::list<int> result;
  result.emplace_front(1, 2, 3, 4, 5, 6, 7, 8, 9);
  int i = 9;
  for (auto it = result.begin(); it != result.end(); ++it) {
    ASSERT_EQ(*it, i);
    --i;
  }
}
