#include <gtest/gtest.h>

#include <map>
#include <set>

#include "../s21_map.h"
#include "../s21_multiset.h"
#include "../s21_set.h"

TEST(iterators_set, true) {
  s21::set<int> a1({3, 2, 6, 85}), a(a1);
  std::set<int> b({3, 6, 2, 85});
  s21::set<int>::iterator iter_s21 = a.begin();
  std::set<int>::iterator iter_std = b.begin();
  for (int i = 0; i < 4; i++) {
    ASSERT_TRUE(*iter_std == *iter_s21);
  }
  iter_s21 = a.end();
  iter_std = b.end();
  --iter_s21;
  --iter_std;
  ASSERT_TRUE(*iter_std == *iter_s21);
}

TEST(contain_set, true) {
  s21::set<int> a({3, 2, 6, 85}), a1;
  std::set<int> b({3, 2, 6, 85}), b1;
  ASSERT_TRUE(a.empty() == b.empty() && a1.empty() == b1.empty());
  ASSERT_TRUE(a.size() == b.size() && a1.size() == b1.size());
  ASSERT_TRUE(a.max_size());
}

TEST(change_set, true) {
  s21::set<int> a({3, 2, 6, 85});
  std::set<int> b({3, 2, 6, 85});
  a.clear();
  b.clear();
  ASSERT_TRUE(a.empty() == b.empty());
  s21::set<int>::iterator iter_s21 = a.insert(5).first;
  std::set<int>::iterator iter_std = b.insert(5).first;
  ASSERT_TRUE(*iter_s21 == *iter_std);
  ASSERT_TRUE(a.insert(5).second == b.insert(5).second);
  ASSERT_TRUE(a.insert(7).second == b.insert(7).second);
  a.erase(a.find(7));
  b.erase(b.find(7));
  ASSERT_TRUE(!a.contains(7));
  ASSERT_TRUE(!a.contains(7));
  a.emplace(10, 11, 12);
  ASSERT_TRUE(a.contains(10) && a.contains(11) && a.contains(12));
}

TEST(erase_dop_set, true) {
  s21::set<int> a({3, 2, 6, 85});
  std::set<int> b({3, 2, 6, 85});

  a.emplace(5, 7);
  b.insert(5);
  b.insert(7);
  a.erase(a.find(7));
  b.erase(b.find(7));
  ASSERT_TRUE(!a.contains(7));
  ASSERT_TRUE(!a.contains(7));
  a.emplace(4, 10, 12);
  a.erase(a.find(85));
  b.erase(b.find(85));
  ASSERT_TRUE(!a.contains(85));
  ASSERT_TRUE(!a.contains(85));

  b.insert(4);
  b.insert(10);
  a.erase(a.find(6));
  b.erase(b.find(6));
  ASSERT_TRUE(!a.contains(6));
  ASSERT_TRUE(!a.contains(6));

  s21::set<int>::iterator it = a.begin();
  ++it;
  --it;
  ASSERT_TRUE(*it == 2);
}

TEST(erase_dop_2_set, true) {
  s21::set<int> a({3});
  std::set<int> b({3});
  a.erase(a.find(3));
  b.erase(b.find(3));
  ASSERT_TRUE(!a.contains(3));
  ASSERT_TRUE(!a.contains(3));
}

TEST(swap_merge_set, true) {
  s21::set<int> a({3, 2, 6, 85}), a1({5, 4, 98});
  a.swap(a1);
  ASSERT_TRUE(a.contains(5) && a.contains(4) && a.contains(98));
  ASSERT_TRUE(a1.contains(3) && a1.contains(2) && a1.contains(85) &&
              a1.contains(6));
  a.merge(a1);
  ASSERT_TRUE(a.contains(3) && a.contains(2) && a.contains(85) &&
              a.contains(6) && a.contains(5) && a.contains(4) &&
              a.contains(98));
}

TEST(iterators_multiset, true) {
  s21::multiset<int> a1({3, 2, 6, 85}), a(a1), a2(a1);
  std::multiset<int> b({3, 6, 2, 85});
  s21::multiset<int>::iterator iter_s21 = a.begin();
  std::multiset<int>::iterator iter_std = b.begin();
  for (int i = 0; i < 4; i++) {
    ASSERT_TRUE(*iter_std == *iter_s21);
  }
  iter_s21 = a.end();
  iter_std = b.end();
  --iter_s21;
  --iter_std;
  ASSERT_TRUE(*iter_std == *iter_s21);
}

TEST(contain_multiset, true) {
  s21::multiset<int> a({3, 2, 2, 6, 85}), a1;
  std::multiset<int> b({3, 2, 2, 6, 85}), b1;
  ASSERT_TRUE(a.empty() == b.empty() && a1.empty() == b1.empty());
  ASSERT_TRUE(a.size() == b.size() && a1.size() == b1.size());
  ASSERT_TRUE(a.max_size());
}

TEST(change_multiset, true) {
  s21::multiset<int> a({3, 2, 6, 85});
  std::multiset<int> b({3, 2, 6, 85});
  a.clear();
  b.clear();
  ASSERT_TRUE(a.empty() == b.empty());
  s21::multiset<int>::iterator iter_s21 = a.insert(5);
  std::multiset<int>::iterator iter_std = b.insert(5);
  ASSERT_TRUE(*iter_s21 == *iter_std);
  a.emplace(5, 5, 5, 5);
  b.insert(5);
  b.insert(5);
  b.insert(5);
  b.insert(5);
  ASSERT_TRUE(a.count(6) == b.count(6));
  ASSERT_TRUE(a.count(5) == b.count(5));
  a.insert(7);
  b.insert(7);
  iter_s21 = a.upper_bound(5);
  iter_std = b.upper_bound(5);
  ASSERT_TRUE(*iter_s21 == *iter_std);
  a.erase(iter_s21);
  b.erase(iter_std);
  ASSERT_TRUE(!a.contains(7));
  ASSERT_TRUE(a.contains(5));
  a.insert(6);
  b.insert(6);
  iter_s21 = a.lower_bound(6);
  iter_std = b.lower_bound(6);
  ASSERT_TRUE(*iter_s21 == *iter_std);
  iter_s21 = a.equal_range(5).first;
  iter_std = b.equal_range(5).first;
  s21::multiset<int>::iterator iter_s21_2 = a.equal_range(5).second;
  std::multiset<int>::iterator iter_std_2 = b.equal_range(5).second;
  ASSERT_TRUE(*iter_s21 == *iter_std && *iter_s21_2 == *iter_std_2);
  ASSERT_TRUE(*a.find(5) == *b.find(5));
}

TEST(swap_merge_multiset, true) {
  s21::multiset<int> a({3, 2, 6, 85}), a1({5, 4, 98, 3, 2});
  a.swap(a1);
  ASSERT_TRUE(a1.contains(3) && a1.contains(2) && a.contains(5) &&
              a.contains(4) && a.contains(98));
  ASSERT_TRUE(a1.contains(3) && a1.contains(2) && a1.contains(85) &&
              a1.contains(6));
  a.merge(a1);
  ASSERT_TRUE(a.contains(3) && a.contains(2) && a.contains(85) &&
              a.contains(6) && a.contains(5) && a.contains(4) &&
              a.contains(98));
}

TEST(iterators_map, true) {
  s21::map<int, int> a1({{3, 2}, {6, 85}}), a(a1);
  std::map<int, int> b({{3, 2}, {6, 85}});
  s21::map<int, int>::iterator iter_s21 = a.begin();
  std::map<int, int>::iterator iter_std = b.begin();
  for (int i = 0; i < 2; i++) {
    ASSERT_TRUE((*iter_std).second == (*iter_s21).second &&
                (*iter_std).first == (*iter_s21).first);
    ++iter_std;
    ++iter_s21;
  }
  iter_s21 = a.end();
  iter_std = b.end();
  --iter_s21;
  --iter_std;
  ASSERT_TRUE((*iter_std).second == (*iter_s21).second &&
              (*iter_std).first == (*iter_s21).first);
  ASSERT_TRUE(a.at(3) == b.at(3));
  ASSERT_TRUE(a[3] == b[3]);
}

TEST(contain_map, true) {
  s21::map<int, int> a({{3, 2}, {6, 85}}), a1;
  std::map<int, int> b({{3, 2}, {6, 85}}), b1;
  ASSERT_TRUE(a.empty() == b.empty() && a1.empty() == b1.empty());
  ASSERT_TRUE(a.size() == b.size() && a1.size() == b1.size());
  ASSERT_TRUE(a.max_size());
}

TEST(change_map, true) {
  s21::map<int, int> a({{3, 2}, {6, 85}});
  std::map<int, int> b({{3, 2}, {6, 85}});
  a.clear();
  b.clear();
  ASSERT_TRUE(a.empty() == b.empty());
  s21::map<int, int>::iterator iter_s21 = a.insert({5, 4}).first;
  std::map<int, int>::iterator iter_std = b.insert({5, 4}).first;
  ASSERT_TRUE((*iter_std).second == (*iter_s21).second &&
              (*iter_std).first == (*iter_s21).first);
  iter_s21 = a.insert(15, 4).first;
  iter_std = b.insert({15, 4}).first;
  ASSERT_TRUE((*iter_std).second == (*iter_s21).second &&
              (*iter_std).first == (*iter_s21).first);
  iter_s21 = a.insert_or_assign(15, 18).first;
  b.at(15) = 18;
  ASSERT_TRUE((*iter_std).second == (*iter_s21).second &&
              (*iter_std).first == (*iter_s21).first);
  a.erase(iter_s21);
  b.erase(iter_std);
  ASSERT_TRUE(!a.contains(7));
  ASSERT_TRUE(a.contains(5));
  std::pair<int, int> test1 = {10, 11};
  std::pair<int, int> test2 = {12, 17};
  a.emplace(test1, test2);
  ASSERT_TRUE(a.contains(10) && a.contains(12));
}

TEST(swap_merge_map, true) {
  s21::map<int, int> a({{3, 2}, {6, 85}}), a1({{2, 3}, {10, 4}});
  a.swap(a1);
  ASSERT_TRUE(a.contains(2) && a.contains(10));
  ASSERT_TRUE(a1.contains(3) && a1.contains(6));
  a.merge(a1);
  ASSERT_TRUE(a.contains(3) && a.contains(2) && a.contains(10) &&
              a.contains(6));
}
