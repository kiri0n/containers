#include "../s21_stack.h"

#include <gtest/gtest.h>

#include <initializer_list>
#include <iostream>
#include <stack>
using std::cout;
using std::endl;

void CompareStackInt(s21::stack<int> &result, std::stack<int> &expect) {
  auto size_result = result.size();
  auto size_expect = expect.size();
  if (size_result == size_expect) {
    while (result.size()) {
      ASSERT_EQ(result.top(), expect.top());
      expect.pop();
      result.pop();
    }
  } else {
    cout << "expect size: " << expect.size()
         << "| result size: " << result.size() << endl;
    ADD_FAILURE();
  }
}

TEST(stack, constructor_default_1) {
  s21::stack<int> result;
  std::stack<int> expect;
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(stack, constructor_initlist_2) {
  std::initializer_list<int> n{};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(stack, constructor_initlist_3) {
  std::initializer_list<int> n{1};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(stack, constructor_initlist_4) {
  std::initializer_list<int> n{1, 2};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(stack, constructor_initlist_5) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(stack, constructor_copy_6) {
  std::initializer_list<int> n{};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  s21::stack<int> result(resultStart);
  std::stack<int> expect(expectStart);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(stack, constructor_copy_7) {
  std::initializer_list<int> n{1};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  s21::stack<int> result(resultStart);
  std::stack<int> expect(expectStart);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(stack, constructor_copy_8) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  s21::stack<int> result(resultStart);
  std::stack<int> expect(expectStart);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(stack, constructor_move_9) {
  std::initializer_list<int> n{};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  s21::stack<int> result(std::move(resultStart));
  std::stack<int> expect(std::move(expectStart));
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(stack, constructor_move_10) {
  std::initializer_list<int> n{1};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  s21::stack<int> result(std::move(resultStart));
  std::stack<int> expect(std::move(expectStart));
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(stack, constructor_move_11) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  s21::stack<int> result(std::move(resultStart));
  std::stack<int> expect(std::move(expectStart));
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(stack, opertor_move_1) {
  std::initializer_list<int> n{};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  s21::stack<int> result;
  std::stack<int> expect;
  result = std::move(resultStart);
  expect = std::move(expectStart);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(stack, opertor_move_2) {
  std::initializer_list<int> n{1};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  s21::stack<int> result;
  std::stack<int> expect;
  result = std::move(resultStart);
  expect = std::move(expectStart);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(stack, opertor_move_3) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  s21::stack<int> result;
  std::stack<int> expect;
  result = std::move(resultStart);
  expect = std::move(expectStart);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(resultStart, expectStart);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(stack, top_1) {
  std::initializer_list<int> n{1};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  ASSERT_EQ(resultStart.top(), expectStart.top());
}

TEST(stack, top_2) {
  std::initializer_list<int> n{1, 2, 3, 4, 5};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  ASSERT_EQ(resultStart.top(), expectStart.top());
}

TEST(stack, empty_1) {
  s21::stack<int> resultStart;
  std::stack<int> expectStart;
  ASSERT_TRUE((resultStart.empty() == expectStart.empty()));
}

TEST(stack, empty_2) {
  std::initializer_list<int> n{4};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
}

TEST(stack, empty_3) {
  std::initializer_list<int> n{1, 2, 3, 4, 5};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  ASSERT_EQ(resultStart.empty(), expectStart.empty());
}

TEST(stack, size_1) {
  s21::stack<int> resultStart;
  std::stack<int> expectStart;
  ASSERT_TRUE((resultStart.size() == expectStart.size()));
}

TEST(stack, size_2) {
  std::initializer_list<int> n{4};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(stack, size_3) {
  std::initializer_list<int> n{1, 2, 3, 4, 5};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(stack, size_4) {
  std::initializer_list<int> n{};
  s21::stack<int> resultStart(n);
  std::stack<int> expectStart(n);
  ASSERT_EQ(resultStart.size(), expectStart.size());
}

TEST(stack, push_1) {
  s21::stack<int> result;
  std::stack<int> expect;
  int value{10};
  int &val = value;
  result.push(val);
  expect.push(val);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(stack, push_2) {
  std::initializer_list<int> n{1};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  int value{10};
  int &val = value;
  result.push(val);
  expect.push(val);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(stack, push_3) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  int value{10};
  int &val = value;
  result.push(val);
  expect.push(val);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(stack, pop_1) {
  std::initializer_list<int> n{1};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  result.pop();
  expect.pop();
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(stack, pop_2) {
  std::initializer_list<int> n{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  result.pop();
  expect.pop();
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
}

TEST(stack, swap_1) {
  std::initializer_list<int> n{};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  s21::stack<int> result1(n);
  std::stack<int> expect1(n);
  result.swap(result1);
  expect.swap(expect1);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(stack, swap_2) {
  std::initializer_list<int> n{};
  std::initializer_list<int> m{5};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  s21::stack<int> result1(m);
  std::stack<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(stack, swap_3) {
  std::initializer_list<int> n{5};
  std::initializer_list<int> m{};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  s21::stack<int> result1(m);
  std::stack<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(stack, swap_4) {
  std::initializer_list<int> n{1};
  std::initializer_list<int> m{5};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  s21::stack<int> result1(m);
  std::stack<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(stack, swap_5) {
  std::initializer_list<int> n{};
  std::initializer_list<int> m{5, 6, 7, 8, 9};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  s21::stack<int> result1(m);
  std::stack<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(stack, swap_6) {
  std::initializer_list<int> n{5, 6, 7, 8, 9};
  std::initializer_list<int> m{};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  s21::stack<int> result1(m);
  std::stack<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(stack, swap_7) {
  std::initializer_list<int> n{1, 2};
  std::initializer_list<int> m{5, 6, 7, 8, 9};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  s21::stack<int> result1(m);
  std::stack<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(stack, swap_8) {
  std::initializer_list<int> m{1, 2};
  std::initializer_list<int> n{5, 6, 7, 8, 9};
  s21::stack<int> result(n);
  std::stack<int> expect(n);
  s21::stack<int> result1(m);
  std::stack<int> expect1(m);
  result.swap(result1);
  expect.swap(expect1);
  CompareStackInt(result, expect);
  ASSERT_EQ(result.empty(), expect.empty());
  ASSERT_EQ(result.size(), expect.size());
  CompareStackInt(result1, expect1);
  ASSERT_EQ(result1.empty(), expect1.empty());
  ASSERT_EQ(result1.size(), expect1.size());
}

TEST(stack, emplace_front_1) {
  std::initializer_list<int> m{1, 2, 3, 4, 5, 6, 7};
  std::initializer_list<int> n{1, 2};
  std::stack<int> expect(m);
  s21::stack<int> result(n);
  result.emplace_front(3, 4, 5, 6, 7);
  ASSERT_EQ(result.size(), 7);
  ASSERT_EQ(result.size(), expect.size());
  ASSERT_EQ(result.top(), 7);
  ASSERT_EQ(result.top(), expect.top());
  CompareStackInt(result, expect);
}

TEST(stack, emplace_front_2) {
  s21::stack<int> result;
  ASSERT_EQ(result.size(), 0);
  result.emplace_front(3, 4, 5, 6, 7);
  ASSERT_EQ(result.size(), 5);
  ASSERT_EQ(result.top(), 7);
  result.pop();
  result.pop();
  ASSERT_EQ(result.size(), 3);
  ASSERT_EQ(result.top(), 5);
  result.emplace_front(8, 9, 10, 11, 12, 13, 14, 15, 16);
  ASSERT_EQ(result.size(), 12);
  ASSERT_EQ(result.top(), 16);
}
