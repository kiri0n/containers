#ifndef CONTAINER_S21_ARRAY_H_
#define CONTAINER_S21_ARRAY_H_
#include <iostream>

namespace s21 {

template <typename T, size_t V>
class array {
 public:
  using value_type = T;
  using reference = T &;
  using const_reference = const T &;
  using iterator = T *;
  using const_iterator = const T *;
  using size_type = size_t;

  array();
  explicit array(std::initializer_list<value_type> const &items);
  array(const array &a);
  array(array &&a);
  ~array();
  array &operator=(array &a);
  array &operator=(array &&a);

  reference at(size_type pos);
  reference operator[](size_type pos);
  const_reference front() const;
  const_reference back() const;
  iterator data();

  iterator begin();
  iterator end();

  const_iterator const_begin() const;
  const_iterator const_end() const;

  bool empty() const;
  size_type size() const;
  size_type max_size() const;

  void swap(array &other);
  void fill(const_reference value);

 private:
  value_type container_[V];
};
#include "s21_array.inl"
}  // namespace s21

#endif  // CONTAINER_S21_ARRAY_H_
