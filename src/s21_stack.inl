template <typename T>
stack<T>::stack() {
  head_ = new NodeStack<T>;
  size_ = 0;
};

template <typename T>
stack<T>::stack(std::initializer_list<T> const &items) : stack() {
  for (auto it = items.begin(); it != items.end(); ++it) {
    push(*it);
  }
}

template <typename T>
stack<T>::stack(const stack &s) : stack() {
  if (s.size_) {
    stack<T> tmp_s;
    NodeStack<T> *tmp = s.head_;
    while (tmp != nullptr) {
      tmp_s.push(tmp->data);
      tmp = tmp->next;
    }
    tmp = tmp_s.head_;
    while (tmp != nullptr) {
      push(tmp->data);
      tmp = tmp->next;
    }
  }
}

template <typename T>
stack<T>::stack(stack &&s) : stack() {
  swap(s);
}

template <typename T>
stack<T>::~stack() {
  while (size_) {
    pop();
  }
  delete head_;
}

template <typename T>
stack<T> &stack<T>::operator=(stack &&s) {
  if (this != &s) {
    while (size_) {
      pop();
    }
    delete head_;
    head_ = s.head_;
    size_ = s.size_;
    s.head_ = nullptr;
    s.size_ = 0;
  }
  return *this;
}

template <typename T>
typename stack<T>::const_reference stack<T>::top() const {
  return head_->data;
}

template <typename T>
bool stack<T>::empty() const {
  return size_ == 0;
}

template <typename T>
typename stack<T>::size_type stack<T>::size() const {
  return size_;
}

template <typename T>
void stack<T>::push(const_reference value) {
  if (!size_) {
    head_->data = value;
    head_->next = nullptr;
  } else {
    NodeStack<T> *tmp = new NodeStack<T>;
    tmp->data = value;
    tmp->next = head_;
    head_ = tmp;
  }
  ++size_;
}

template <typename T>
void stack<T>::pop() {
  if (size_) {
    NodeStack<T> *tmp = head_->next;
    delete head_;
    head_ = tmp;
    --size_;
  }
}

template <typename T>
void stack<T>::swap(stack &s) {
  std::swap(head_, s.head_);
  std::swap(size_, s.size_);
}

template <typename T>
template <class... Args>
void stack<T>::emplace_front(Args &&...args) {
  value_type data[] = {args...};
  for (value_type items : data) {
    push(items);
  }
}
