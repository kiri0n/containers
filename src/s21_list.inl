template <typename T>
list<T>::list() : head_(nullptr), tail_(nullptr), size_(0) {
  head_ = tail_ = new Node<T>;
}

template <typename T>
list<T>::list(size_type n) : list() {
  if (n > max_size()) std::length_error("Out of available memory");
  for (size_type i = 0; i < n; ++i) push_back(0);
}

template <typename T>
list<T>::list(std::initializer_list<T> const &items) : list() {
  for (auto i = items.begin(); i != items.end(); ++i) {
    push_back(*i);
  }
}

template <typename T>
list<T>::list(const list &l) : list() {
  for (auto i = l.begin(); i != l.end(); ++i) {
    push_back(*i);
  }
}

template <typename T>
list<T>::list(list &&l) : list() {
  swap(l);
}

template <typename T>
list<T>::~list() {
  clear();
}

template <typename T>
list<T> &list<T>::operator=(list &&l) {
  if (this != &l) {
    clear();
    head_ = l.head_;
    tail_ = l.tail_;
    size_ = l.size_;
    l.head_ = nullptr;
    l.tail_ = nullptr;
    l.size_ = 0;
  }
  return *this;
}

template <typename T>
typename list<T>::size_type list<T>::max_size() const {
  return std::numeric_limits<size_type>::max() / sizeof(Node<T>) / 2;
}

template <typename T>
bool list<T>::empty() const {
  return size_ == 0;
}

template <typename T>
typename list<T>::size_type list<T>::size() const {
  return size_;
}

template <typename T>
typename list<T>::iterator list<T>::begin() const {
  return iterator(head_);
}

template <typename T>
typename list<T>::iterator list<T>::end() const {
  return iterator(tail_);
}

template <typename T>
void list<T>::clear() {
  while (tail_) {
    Node<T> *tmp = tail_;
    tail_ = tail_->prev;
    delete tmp;
  }
  head_ = nullptr;
  size_ = 0;
}

template <typename T>
void list<T>::push_back(const_reference value) {
  Node<T> *tmp = new Node<T>;
  tmp->data = value;
  tmp->prev = tail_->prev;
  tmp->next = tail_;
  if (tail_->prev) {
    tail_->prev->next = tmp;
  } else {
    head_ = tmp;
  }
  tail_->prev = tmp;
  ++size_;
}

template <typename T>
void list<T>::swap(list &other) {
  std::swap(head_, other.head_);
  std::swap(tail_, other.tail_);
  std::swap(size_, other.size_);
}
template <typename T>
typename list<T>::const_reference list<T>::front() const {
  if (!size_) throw std::out_of_range("Attempt to access uninitialized memory");
  return head_->data;
}

template <typename T>
typename list<T>::const_reference list<T>::back() const {
  if (!size_) throw std::out_of_range("Attempt to access uninitialized memory");
  return tail_->prev->data;
}

template <typename T>
typename list<T>::iterator list<T>::insert(iterator pos,
                                           const_reference value) {
  Node<T> *tmp = new Node<T>;
  tmp->data = value;
  tmp->prev = pos.ptr_->prev;
  tmp->next = pos.ptr_;
  if (pos.ptr_->prev) {
    pos.ptr_->prev->next = tmp;
  } else {
    head_ = tmp;
  }
  pos.ptr_->prev = tmp;
  ++size_;
  return iterator(tmp);
}

template <typename T>
void list<T>::erase(iterator pos) {
  if (pos.ptr_ && pos.ptr_->next) {
    if (pos.ptr_->prev)
      pos.ptr_->prev->next = pos.ptr_->next;
    else
      head_ = pos.ptr_->next;
    pos.ptr_->next->prev = pos.ptr_->prev;
    --size_;
    delete pos.ptr_;
  }
}

template <typename T>
void list<T>::push_front(const_reference value) {
  insert(begin(), value);
}

template <typename T>
void list<T>::pop_front() {
  if (head_) {
    erase(begin());
  }
}

template <typename T>
void list<T>::pop_back() {
  if (tail_->prev) {
    erase(iterator(tail_->prev));
  }
}

template <typename T>
void list<T>::merge(list &other) {
  auto other_it = other.begin();
  auto this_it = begin();
  if (head_ != other.head_ && tail_ != other.tail_) {
    if (size_) {
      while (true) {
        if (other_it == other.end()) {
          break;
        }
        if (this_it == end()) {
          push_back(*other_it);
          ++other_it;
        } else if (*this_it > *other_it) {
          insert(this_it, *other_it);
          ++other_it;
        } else {
          ++this_it;
        }
      }
    } else {
      swap(other);
    }
    other.clear();
  }
}

template <typename T>
void list<T>::splice(const_iterator pos, list &other) {
  if (size_) {
    for (auto j = other.begin(); j != other.end(); ++j) insert(pos, *j);
  } else {
    swap(other);
  }
}

template <typename T>
void list<T>::reverse() {
  auto start = begin(), finish = iterator(tail_->prev ? tail_->prev : tail_);
  while (start != finish) {
    std::swap(start.ptr_->data, finish.ptr_->data);
    ++start;
    if (start == finish) break;
    --finish;
  }
}

template <typename T>
void list<T>::unique() {
  for (auto i = begin(); i != end(); ++i)
    while (i.ptr_->next->next && i.ptr_->data == i.ptr_->next->data)
      erase(iterator(i.ptr_->next));
}

template <typename T>
void list<T>::sort() {
  for (auto i = begin(); i != end(); ++i) {
    auto j = i;
    ++j;
    for (; j != end(); ++j)
      if (*i > *j) std::swap(i.ptr_->data, j.ptr_->data);
  }
}

template <typename T>
template <typename... Args>
typename list<T>::iterator list<T>::emplace(const_iterator pos,
                                            Args &&...args) {
  iterator tmp = (iterator)pos;
  value_type data[] = {args...};
  size_t len = sizeof(data) / sizeof(value_type);
  for (size_t i = 0; i < len; ++i) {
    tmp = insert(tmp, data[i]);
  }
  return tmp;
}

template <typename T>
template <class... Args>
void list<T>::emplace_back(Args &&...args) {
  value_type data[] = {args...};
  for (value_type items : data) {
    push_back(items);
  }
}

template <typename T>
template <class... Args>
void list<T>::emplace_front(Args &&...args) {
  value_type data[] = {args...};
  for (value_type items : data) {
    push_front(items);
  }
}
