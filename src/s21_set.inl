template <typename K>
typename set<K>::iterator set<K>::find(const K &key) {
  iterator tmp = this->find_key(key, this->ptr);
  if (tmp.iter->Key == key) {
    iterator result;
    result = tmp;
    return result;
  } else {
    return this->end();
  }
}

template <typename K>
set<K>::set() {
  NodeTree<K> *tmp = this->ptr;
  this->ptr = tmp->Construct();
}

template <typename K>
set<K>::set(std::initializer_list<value_type> const &items) : set<K>::set() {
  for (auto i = items.begin(); i != items.end(); ++i) {
    this->insert(*i);
  }
}

template <typename K>
set<K>::set(const set &s) : set<K>::set() {
  iterator it = s.begin();
  while (it != s.end()) {
    this->insert(*it);
    ++it;
  }
}

template <typename K>
set<K>::set(set &&s) {
  this->ptr = s->ptr;
  s->ptr = nullptr;
  this->size_tree = s->size_tree;
  s->size_tree = 0;
}

template <typename K>
set<K>::~set() {
  if (this->ptr != nullptr) this->deleteTree();
}

template <typename K>
set<K> set<K>::operator=(set &&s) {
  this->deleteTree();
  this->ptr = s->ptr;
  s->ptr = nullptr;
  return *this;
}

template <typename K>
template <typename... Args>
std::vector<std::pair<typename set<K>::iterator, bool>> set<K>::emplace(
    Args &&...args) {
  std::vector<std::pair<iterator, bool>> result;
  K arr[] = {args...};
  size_t x = 0;
  for (K i : arr) {
    result.push_back(this->insert(i));
    x++;
  }
  return result;
}
