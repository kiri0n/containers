#ifndef CONTAINER_S21_LIST_H_
#define CONTAINER_S21_LIST_H_

#include <cstddef>
#include <exception>
#include <initializer_list>
#include <limits>
#include <ostream>
#include <utility>

namespace s21 {
template <typename T>
struct Node {
  T data;
  Node<T> *prev, *next;
  Node() : data(), prev(nullptr), next(nullptr) {}
  template <class... Args>
  explicit Node(Args &&...args)
      : data(std::forward<Args>(args)...), prev(nullptr), next(nullptr) {}
};

template <typename T>
class list {
 public:
  class ListIterator;
  using value_type = T;
  using reference = T &;
  using const_reference = const T &;
  using iterator = ListIterator;
  using const_iterator = const ListIterator;
  using size_type = size_t;

  class ListIterator {
   public:
    explicit ListIterator(Node<T> *ptr_tmp) : ptr_(ptr_tmp) {}
    T operator*() const { return ptr_->next ? ptr_->data : 0; }
    void operator++() { ptr_ = ptr_->next ? ptr_->next : nullptr; }
    void operator--() { ptr_ = ptr_->prev ? ptr_->prev : nullptr; }
    bool operator==(iterator other) const { return ptr_ == other.ptr_; }
    bool operator!=(iterator other) const { return ptr_ != other.ptr_; }
    Node<T> *ptr_;
  };
  list();
  explicit list(size_type n);
  explicit list(std::initializer_list<value_type> const &items);
  list(const list &l);
  list(list &&l);
  ~list();
  list &operator=(list &&l);

  const_reference front() const;
  const_reference back() const;

  iterator begin() const;
  iterator end() const;

  bool empty() const;
  size_t size() const;
  size_t max_size() const;

  void clear();
  iterator insert(iterator pos, const_reference value);
  void erase(iterator pos);
  void push_back(const_reference value);
  void pop_back();
  void push_front(const_reference value);
  void pop_front();
  void swap(list &other);
  void merge(list &other);
  void splice(const_iterator pos, list &other);
  void reverse();
  void unique();
  void sort();

  template <typename... Args>
  iterator emplace(const_iterator pos, Args &&...args);
  template <class... Args>
  void emplace_back(Args &&...args);
  template <class... Args>
  void emplace_front(Args &&...args);

 private:
  Node<T> *head_;
  Node<T> *tail_;
  size_type size_;
};

#include "s21_list.inl"
}  // namespace s21
#endif  // CONTAINER_S21_LIST_H_
