#ifndef CONTAINER_S21_SET_H_
#define CONTAINER_S21_SET_H_
#include "s21_tree.h"

namespace s21 {

template <typename K>
class set : public Tree<K> {
 public:
  class TreeIterator;
  using thisclass = set;
  using iterator = typename Tree<K>::TreeIterator;
  using key_type = K;
  using value_type = K;
  using reference = value_type &;
  using const_reference = const value_type &;
  using size_type = size_t;
  using Node = NodeTree<K>;

  set();
  explicit set(std::initializer_list<value_type> const &items);
  set(const set &s);
  set(set &&s);
  ~set();
  set operator=(set &&s);

  iterator find(const K &key);

  template <typename... Args>
  std::vector<std::pair<iterator, bool>> emplace(Args &&...args);
};

#include "s21_set.inl"
}  // namespace s21
#endif // CONTAINER_S21_SET_H_