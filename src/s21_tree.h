#ifndef CONTAINER_S21_TREE_H_
#define CONTAINER_S21_TREE_H_

#include <iostream>
#include <vector>

namespace s21 {

enum Node_stat { end_node = 1, begin_node = -1, middle_node = 0 };

template <typename K>
struct NodeTree {
  using Node = NodeTree;
  K Key;
  NodeTree<K> *parent, *left, *right;
  Node_stat status;
  template <class... Args>
  explicit NodeTree(Args &&...args)
      : Key(std::forward<Args>(args)...),
        parent(nullptr),
        left(nullptr),
        right(nullptr),
        status(end_node){};
  void CreateNewNode(K value, bool side);
  void Erase_Edge_Node();
  void Erase_Mid_Node(NodeTree<K> *child);
  void Erase_Prev_End(NodeTree<K> const *ptr);
  NodeTree<K> *Construct();
};

template <typename K>
class Tree {
 public:
  class TreeIterator;
  using thisclass = Tree;
  using iterator = TreeIterator;
  using const_iterator = const TreeIterator;
  using key_type = K;
  using value_type = K;
  using reference = value_type &;
  using const_reference = const value_type &;
  using size_type = size_t;
  using Node = NodeTree<K>;

  iterator begin() const;
  iterator end() const;

  bool empty();
  size_type size();
  size_type max_size();

  void clear();

  void erase(iterator pos);
  void swap(thisclass &other);
  void merge(thisclass &other);
  bool contains(const K &key);
  std::pair<iterator, bool> insert(const value_type &value);

 protected:
  void deleteTree();
  iterator find_key(const value_type &value, Node *ptrf);
  Node *ptr = nullptr;
  size_type size_tree = 0;
  std::pair<iterator, bool> sup_insert(const value_type &value);
};

template <typename K>
class Tree<K>::TreeIterator {
 public:
  Node *iter;

  ~TreeIterator();
  K &operator*() const;
  iterator &operator++();
  iterator &operator--();
  bool operator==(iterator other) const;
  bool operator!=(iterator other) const;
};

#include "s21_tree.inl"

}  // namespace s21

#endif  // CONTAINER_S21_TREE_H_
