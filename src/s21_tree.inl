template <typename K>
void Tree<K>::deleteTree() {
  iterator it = begin();
  size_t sz = size_tree + 1;
  if (size_tree == 0) sz++;
  NodeTree<K> *arr[sz];
  for (size_t i = 0; i <= sz - 1; i++) {
    arr[i] = it.iter;
    ++it;
  }
  for (size_t i = 0; i <= sz - 1; i++) {
    if (arr[i]) delete arr[i];
  }
  ptr = nullptr;
  size_tree = 0;
}

template <typename K>
Tree<K>::iterator::~TreeIterator() {
  iter = nullptr;
}

template <typename K>
typename Tree<K>::iterator &Tree<K>::iterator::operator++() {
  if (iter && iter->status != end_node) {
    if (iter->right != nullptr) {
      iter = iter->right;
      while (iter->left != nullptr && iter->status != end_node) {
        iter = iter->left;
      }
    } else if (iter->parent != nullptr) {
      Node *tmp = iter;
      iter = iter->parent;

      while (iter->right == tmp) {
        tmp = iter;
        iter = iter->parent;
      }
    }
  }
  return *this;
}

template <typename K>
typename Tree<K>::iterator &Tree<K>::iterator::operator--() {
  if (iter->left != nullptr) {
    iter = iter->left;
    while (iter->right != nullptr) {
      iter = iter->right;
    }
  } else if (iter->parent != nullptr) {
    Node *begin = iter;
    Node *tmp = iter;
    iter = iter->parent;
    while (iter->left == tmp) {
      tmp = iter;
      if (iter->parent == nullptr) {
        iter = begin;
        break;
      }
      iter = iter->parent;
    }
  }
  return *this;
}

template <typename K>
bool Tree<K>::iterator::operator==(iterator other) const {
  return iter == other.iter;
}

template <typename K>
bool Tree<K>::iterator::operator!=(iterator other) const {
  return iter != other.iter;
}

template <typename K>
K &Tree<K>::iterator::operator*() const {
  if (iter && iter->status == end_node) {
    throw std::out_of_range("Iterator points to End()");
  } else {
    return iter->Key;
  }
}

template <typename K>
void NodeTree<K>::CreateNewNode(K value, bool side) {
  Node *NewNode = new Node;
  NewNode->status = middle_node;
  NewNode->parent = this;
  if (side == 0) {
    left = NewNode;
    NewNode->Key = value;
  } else {
    if (right && right->status == end_node) {
      right->status = middle_node;
      right->Key = value;
      right->right = NewNode;
      NewNode->status = end_node;
      NewNode->parent = right;
    } else {
      right = NewNode;
      NewNode->Key = value;
    }
  }
}

template <typename K>
void NodeTree<K>::Erase_Edge_Node() {
  if (parent->left == this) {
    parent->left = nullptr;
  } else {
    parent->right = nullptr;
  }
  delete this;
}

template <typename K>
void NodeTree<K>::Erase_Mid_Node(NodeTree<K> *child) {
  child->parent = parent;
  if (parent->left == this) {
    parent->left = child;
  } else {
    parent->right = child;
  }
  delete this;
}

template <typename K>
void NodeTree<K>::Erase_Prev_End(NodeTree<K> const *ptr) {
  NodeTree<K> *max_left;
  max_left = left;
  if (ptr == this) {
    max_left->parent = nullptr;
  } else {
    max_left->parent = parent;
    parent->right = max_left;
  }
  delete right;
  delete this;
  while (max_left->right != nullptr) {
    max_left = max_left->right;
  }
  max_left->right = new Node;
  max_left->right->status = end_node;
  max_left->right->parent = max_left;
}

template <typename K>
NodeTree<K> *NodeTree<K>::Construct() {
  Node *NodeEnd = new Node;
  Node *NodeBegin = new Node;
  NodeBegin->status = middle_node;
  NodeBegin->right = NodeEnd;
  NodeEnd->parent = NodeBegin;
  return NodeBegin;
}

template <typename K>
typename Tree<K>::iterator Tree<K>::begin() const {
  Node *begin = ptr;
  while (size_tree != 0 && begin->left != nullptr) begin = begin->left;
  iterator tmp;
  tmp.iter = begin;
  return tmp;
}

template <typename K>
typename Tree<K>::iterator Tree<K>::end() const {
  Node *begin = ptr;
  while (begin->right != nullptr) begin = begin->right;
  iterator tmp;
  tmp.iter = begin;
  return tmp;
}

template <typename K>
bool Tree<K>::empty() {
  return size_tree == 0;
}

template <typename K>
typename Tree<K>::size_type Tree<K>::size() {
  return size_tree;
}

template <typename K>
typename Tree<K>::size_type Tree<K>::max_size() {
  return SIZE_MAX / sizeof(Node) / 2;
  ;
}

template <typename K>
void Tree<K>::clear() {
  deleteTree();
  Node *tmp = this->ptr;
  this->ptr = tmp->Construct();
}

template <typename K>
void Tree<K>::swap(thisclass &other) {
  Node *tmp = other.ptr;
  other.ptr = ptr;
  size_t a = size_tree;
  size_tree = other.size_tree;
  other.size_tree = a;
  ptr = tmp;
}

template <typename K>
void Tree<K>::merge(thisclass &other) {
  Tree<K>::iterator it = other.begin();
  while (it != other.end() && other.size_tree != 0) {
    insert(*it);
    ++it;
  }
  other.deleteTree();
}

template <typename K>
void Tree<K>::erase(iterator pos) {
  if (size_tree == 0 || pos.iter->status == end_node) {
    throw std::out_of_range("Erase if Empty Contain or Erase End Node");
  }
  Node *tmp = pos.iter;
  if (tmp->right == nullptr && tmp->left == nullptr) {
    tmp->Erase_Edge_Node();
  } else if (tmp->right == nullptr || tmp->left == nullptr) {
    if (tmp->right != nullptr) {
      if (tmp->parent == nullptr) {
        if (tmp->right->status == end_node) {
          ptr = new Node;
          ptr->status = middle_node;
          ptr->right = tmp->right;
          tmp->right->parent = ptr;
          delete tmp;
        } else {
          ptr = tmp->right;
          delete tmp;
          ptr->parent = nullptr;
        }
      } else {
        tmp->Erase_Mid_Node(tmp->right);
      }
    } else {
      tmp->Erase_Mid_Node(tmp->left);
    }
  } else {
    if (tmp->right->status == end_node) {
      tmp->Erase_Prev_End(ptr);
    } else {
      Node *max_left = tmp;
      max_left = max_left->right;
      while (max_left->left != nullptr) max_left = max_left->left;
      tmp->Key = max_left->Key;
      iterator max_n = begin();
      max_n.iter = max_left;
      erase(max_n);
      ++size_tree;
    }
  }
  --size_tree;
}

template <typename K>
std::pair<typename Tree<K>::iterator, bool> Tree<K>::sup_insert(
    const value_type &value) {
  bool result = true;
  thisclass::iterator it = begin();
  it.iter = ptr;
  std::pair<typename Tree<K>::iterator, bool> res;

  if (size_tree != 0) {
    iterator tmp = find_key(value, ptr);
    Node *OldNode = tmp.iter;
    if (tmp.iter->Key != value) {
      if (value < OldNode->Key) {
        OldNode->CreateNewNode(value, 0);
        it.iter = OldNode->left;
      } else if (value > OldNode->Key) {
        OldNode->CreateNewNode(value, 1);
        it.iter = OldNode->right;
      }
    } else {
      result = false;
      it = tmp;
    }
  } else {
    ptr->Key = value;
  }
  if (result == true) {
    ++size_tree;
  }
  return res = std::make_pair(it, result);
}

template <typename K>
typename Tree<K>::iterator Tree<K>::find_key(const value_type &value,
                                             Node *ptrf) {
  Node *find = ptrf;
  Node *last = ptrf;
  bool next_path = this->size_tree != 0;
  while (next_path == true) {
    if (value == find->Key) {
      last = find;
      next_path = false;
    }
    if (value < find->Key) {
      if (find->left != nullptr) {
        find = find->left;
      } else {
        next_path = false;
      }
    } else {
      if (find->right != nullptr && find->right->status != end_node) {
        find = find->right;
      } else {
        next_path = false;
      }
    }
  }
  iterator res = begin();
  if (last->Key == value)
    res.iter = last;
  else
    res.iter = find;
  return res;
}

template <typename K>
bool Tree<K>::contains(const K &key) {
  iterator tmp = find_key(key, ptr);
  return tmp.iter->Key == key;
}

template <typename K>
std::pair<typename Tree<K>::iterator, bool> Tree<K>::insert(
    const value_type &value) {
  return this->sup_insert(value);
}
