#ifndef CONTAINER_S21_VECTOR_H_
#define CONTAINER_S21_VECTOR_H_
#include <cstddef>
#include <exception>
#include <initializer_list>
#include <iostream>
#include <limits>
#include <ostream>
#include <utility>

namespace s21 {
template <typename T, typename Allocator = std::allocator<T>>
class vector {
 public:
  using value_type = T;
  using allocator_type = Allocator;
  using reference = value_type &;
  using const_reference = const value_type &;
  using iterator = T *;
  using const_iterator = const T *;
  using size_type = size_t;

  vector();
  explicit vector(size_type n);
  explicit vector(std::initializer_list<value_type> const &items);
  vector(const vector &v);
  vector(vector &&v);
  ~vector();
  vector &operator=(const vector &v);
  vector &operator=(vector &&v);

  reference at(size_type pos);
  reference operator[](size_type pos);
  const_reference front() const;
  const_reference back() const;
  iterator data();

  iterator begin();
  iterator end();
  const_iterator const_begin();
  const_iterator const_end();

  bool empty() const;
  size_type size() const;
  size_type max_size() const;
  void reserve(size_type size);
  size_type capacity() const;
  void shrink_to_fit();

  void clear();
  iterator insert(iterator const pos, const_reference value);
  void erase(iterator const pos);
  void push_back(const_reference value);
  void pop_back();
  void swap(vector<value_type> &other);

  template <typename... Args>
  iterator emplace(const_iterator pos, Args &&...args);
  template <typename... Args>
  void emplace_back(Args &&...args);

 private:
  allocator_type c_allocation_;
  size_type c_size_;
  size_type c_capacity_;
  T *container_;
};

#include "s21_vector.inl"
}  // namespace s21

#endif  //  CONTAINER_S21_VECTOR_H_
