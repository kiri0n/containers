#ifndef CONTAINER_S21_QUEUE_H_
#define CONTAINER_S21_QUEUE_H_

#include <cstddef>
#include <initializer_list>
#include <utility>

namespace s21 {
template <typename T>
struct NodeQueue {
  T data;
  NodeQueue<T> *prev;
  NodeQueue() : data(), prev(nullptr) {}
  template <class... Args>
  explicit NodeQueue(Args &&...args)
      : data(std::forward<Args>(args)...), prev(nullptr) {}
};

template <typename T>
class queue {
 public:
  using value_type = T;
  using reference = T &;
  using const_reference = const T &;
  using size_type = size_t;

  queue();
  explicit queue(std::initializer_list<T> const &items);
  queue(const queue &q);

  queue(queue &&q);

  ~queue();
  queue<T> &operator=(queue &&q);

  const_reference front() const;
  const_reference back() const;

  bool empty() const;
  size_t size() const;

  void push(const T &value);
  void pop();
  void swap(queue &other);

  template <class... Args>
  void emplace_back(Args &&...args);

 private:
  size_type size_;
  NodeQueue<T> *head_, *tail_;
};

#include "s21_queue.inl"

}  // namespace s21

#endif  // CONTAINER_S21_QUEUE_H_
