
template <typename T>
queue<T>::queue() {
  head_ = new NodeQueue<T>;
  tail_ = nullptr;
  size_ = 0;
};

template <typename T>
queue<T>::queue(std::initializer_list<T> const &items) : queue() {
  for (auto it = items.begin(); it != items.end(); ++it) {
    push(*it);
  }
}

template <typename T>
queue<T>::queue(const queue &q) : queue() {
  if (q.size_) {
    NodeQueue<T> *tmp = q.head_;
    while (tmp != nullptr) {
      push(tmp->data);
      tmp = tmp->prev;
    }
  }
}

template <typename T>
queue<T>::queue(queue &&q) : queue() {
  swap(q);
}

template <typename T>
queue<T>::~queue() {
  while (size_) {
    pop();
  }
  delete head_;
}

template <typename T>
queue<T> &queue<T>::operator=(queue &&q) {
  if (this != &q) {
    while (size_) {
      pop();
    }
    delete head_;
    head_ = q.head_;
    tail_ = q.tail_;
    size_ = q.size_;
    q.head_ = nullptr;
    q.tail_ = nullptr;
    q.size_ = 0;
  }
  return *this;
}

template <typename T>
void queue<T>::swap(queue &q) {
  std::swap(head_, q.head_);
  std::swap(tail_, q.tail_);
  std::swap(size_, q.size_);
}

template <typename T>
void queue<T>::pop() {
  if (size_) {
    NodeQueue<T> *tmp = head_->prev;
    delete head_;
    head_ = tmp;
    --size_;
  }
}

template <typename T>
void queue<T>::push(const_reference value) {
  if (!size_) {
    head_->data = value;
    head_->prev = nullptr;
    tail_ = head_;
  } else {
    NodeQueue<T> *tmp = new NodeQueue<T>;
    tmp->data = value;
    tmp->prev = tail_->prev;
    tail_->prev = tmp;
    tail_ = tmp;
  }
  ++size_;  // увеличиваем размер
}

template <typename T>
bool queue<T>::empty() const {
  return size_ == 0;
}

template <typename T>
typename queue<T>::size_type queue<T>::size() const {
  return size_;
}

template <typename T>
typename queue<T>::const_reference queue<T>::front() const {
  if (!size_) throw std::out_of_range("Attempt to access uninitialized memory");
  return head_->data;
}

template <typename T>
typename queue<T>::const_reference queue<T>::back() const {
  if (!size_) throw std::out_of_range("Attempt to access uninitialized memory");
  return tail_->data;
}

template <typename T>
template <class... Args>
void queue<T>::emplace_back(Args &&...args) {
  value_type data[] = {args...};
  for (value_type i : data) {
    push(i);
  }
}
