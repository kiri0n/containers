#ifndef CONTAINER_S21_MULTISET_H_
#define CONTAINER_S21_MULTISET_H_

#include "s21_set.h"

namespace s21 {

template <typename K>
class multiset : public set<K> {
 public:
  class TreeIterator;
  using thisclass = multiset;
  using iterator = typename Tree<K>::TreeIterator;
  using key_type = K;
  using value_type = K;
  using reference = value_type &;
  using const_reference = const value_type &;
  using size_type = size_t;
  using Node = NodeTree<K>;

  multiset();
  explicit multiset(std::initializer_list<value_type> const &items);
  multiset(const multiset &s);
  multiset(multiset &&s);
  ~multiset();
  multiset operator=(multiset &&s);

  iterator insert(const value_type &value);
  size_type count(const K &key);
  std::pair<iterator, iterator> equal_range(const K &key);
  iterator lower_bound(const K &key);
  iterator upper_bound(const K &key);

  template <typename... Args>
  std::vector<iterator> emplace(Args &&...args);
};

#include "s21_multiset.inl"

}  // namespace s21
#endif // CONTAINER_S21_MULTISET_H_