template <typename K>
multiset<K>::multiset() : set<K>::set() {}

template <typename K>
multiset<K>::multiset(std::initializer_list<value_type> const &items) {
  for (auto i = items.begin(); i != items.end(); ++i) {
    this->insert(*i);
  }
}

template <typename K>
multiset<K>::multiset(const multiset &s) : multiset<K>::multiset() {
  iterator it = s.begin();
  while (it != s.end()) {
    insert(*it);
    ++it;
  }
}

template <typename K>
multiset<K>::multiset(multiset &&s) : multiset<K>::multiset() {
  this->ptr = s->ptr;
  s->ptr = nullptr;
  this->size_tree = s->size_tree;
  s->size_tree = 0;
}

template <typename K>
multiset<K>::~multiset() {
  this->deleteTree();
}

template <typename K>
multiset<K> multiset<K>::operator=(multiset &&s) {
  this->deleteTree();
  this->ptr = s->ptr;
  s->ptr = nullptr;
  return *this;
}

template <typename K>
typename multiset<K>::iterator multiset<K>::insert(const value_type &value) {
  Node *ins = this->ptr;
  while (1) {
    if (this->size_tree == 0) {
      ins->Key = value;
      break;
    }
    if (value < ins->Key) {
      if (ins->left != nullptr) {
        ins = ins->left;
      } else {
        ins->CreateNewNode(value, 0);
        break;
      }
    } else {
      if (ins->right != nullptr && ins->right->status != end_node) {
        ins = ins->right;
      } else {
        ins->CreateNewNode(value, 1);
        break;
      }
    }
  }
  this->size_tree++;
  iterator tmp = this->begin();
  tmp.iter = ins;
  return tmp;
}

template <typename K>
typename multiset<K>::size_type multiset<K>::count(const K &key) {
  iterator find;
  size_type count_key = 0;
  find = this->find_key(key, this->ptr);
  while (*find == key) {
    ++count_key;
    find = this->find_key(key, find.iter->right);
    if (find.iter->right == nullptr || find.iter->right->status == end_node) {
      if (*find == key) ++count_key;
      break;
    }
  }
  return count_key;
}

template <typename K>
typename multiset<K>::iterator multiset<K>::lower_bound(const K &key) {
  iterator find;
  find = this->find_key(key, this->ptr);
  while (find.iter->status != end_node && *find < key) {
    ++find;
  }
  return find;
}

template <typename K>
typename multiset<K>::iterator multiset<K>::upper_bound(const K &key) {
  iterator find;
  find = this->find_key(key, this->ptr);
  while (find.iter->status != end_node && *find <= key) {
    ++find;
  }
  return find;
}

template <typename K>
std::pair<typename multiset<K>::iterator, typename multiset<K>::iterator>
multiset<K>::equal_range(const K &key) {
  std::pair<iterator, iterator> result;
  result = std::make_pair(lower_bound(key), upper_bound(key));
  return result;
}

template <typename K>
template <typename... Args>
std::vector<typename multiset<K>::iterator> multiset<K>::emplace(
    Args &&...args) {
  std::vector<iterator> result;
  K arr[] = {args...};
  size_t x = 0;
  for (K i : arr) {
    result.push_back(insert(i));
    x++;
  }
  return result;
}
