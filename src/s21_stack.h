#ifndef CONTAINER_S21_STACK_H_
#define CONTAINER_S21_STACK_H_

#include <cstddef>
#include <initializer_list>
#include <utility>

namespace s21 {
template <typename T>
struct NodeStack {
  T data;
  NodeStack<T> *next;
  NodeStack() : data(), next(nullptr) {}
};
template <typename T>
class stack {
 public:
  using value_type = T;
  using reference = T &;
  using const_reference = const T &;
  using size_type = size_t;

  stack();
  explicit stack(std::initializer_list<T> const &items);
  stack(const stack &s);
  stack(stack &&s);
  ~stack();
  stack<T> &operator=(stack &&s);

  const_reference top() const;

  bool empty() const;
  size_t size() const;

  void push(const T &value);
  void pop();
  void swap(stack &other);

  template <class... Args>
  void emplace_front(Args &&...args);

 private:
  size_type size_;
  NodeStack<T> *head_;
};

#include "s21_stack.inl"

}  // namespace s21

#endif  // CONTAINER_S21_STACK_H_
